<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 01.08.2019
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Main Page</title>
</head>
<body>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <h2>Main Page</h2>
    <form class="form-inline">
        <a class="btn btn-outline-success" href="controller?command=to_main"><fmt:message key="main"/></a>
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_groups"><fmt:message key="groups"/></a>
        <c:if test="${user.role == 'ADMIN'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_users"><fmt:message
                    key="users"/></a>
        </c:if>
        <c:if test="${user.role != 'GUEST'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_user"><fmt:message
                    key="Profile"/></a>
        </c:if>
        <%--  <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_training"><fmt:message key="Training" /></a>
          <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_error"><fmt:message key="Error" /></a>--%>
        <c:choose>
            <c:when test="${user.role == 'GUEST'}">
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_login"><fmt:message
                        key="Login"/></a>
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_registration"><fmt:message
                        key="Registration"/></a>
            </c:when>
            <c:when test="${user.role != 'GUEST'}">
                <button class="btn btn-sm btn-outline-secondary" type="submit" formaction="controller?command=logout"
                        formmethod="post"><fmt:message key="Logout"/>
                </button>
            </c:when>
        </c:choose>
        <a class="btn btn-default " href="controller?lang=en&command=language">ENG</a>
        <a class="btn btn-default" href="controller?lang=ru&command=language">RUS</a>
    </form>
</nav>

<table class="table table-striped">
    <thead><fmt:message key="main.courses"/>
    <tr>
        <th scope="col"><fmt:message key="Course.ID"/></th>
        <th scope="col"><fmt:message key="Course.name"/></th>
        <th scope="col"><fmt:message key="Course.Rank"/></th>
        <th scope="col"><fmt:message key="Course.rating"/></th>
        <th scope="col"><fmt:message key="Direction.name"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="course" items="${courses}">
        <tr>
            <th scope="row">${course.idCourse}</th>
            <td>${course.name}</td>
            <td>${course.rank}</td>
            <td>${course.rating}</td>
            <td>${course.direction.name}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<table class="table table-striped">
    <thead><fmt:message key="main.trainers"/>
    <tr>
        <th scope="col"><fmt:message key="Trainer.ID"/></th>
        <th scope="col"><fmt:message key="First.name"/></th>
        <th scope="col"><fmt:message key="Second.name"/></th>
        <th scope="col"><fmt:message key="Rank"/></th>
        <th scope="col"><fmt:message key="Direction.name"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="trainer" items="${trainers}">
        <tr>
            <th scope="row">${trainer.idTrainer}</th>
            <td>${trainer.firstName}</td>
            <td>${trainer.secondName}</td>
            <td>${trainer.rank}</td>
            <td>${trainer.direction.name}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<c:if test="${user.role == 'ADMIN'}">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=ADD_COURSE">
                    <h2 class="text-center"><fmt:message key="Course.adding"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="directionID"><fmt:message key="Direction.ID"/></label>
                            <div class="col-xs-8" data-tip="directionID ">
                                <input type="number" id="directionID" name="directionID"
                                       placeholder="<fmt:message key="Direction.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="courseName"><fmt:message key="Course.name"/> </label>
                            <div class="col-xs-8" data-tip="courseName ">
                                <input type="text" id="courseName" name="courseName"
                                       placeholder="<fmt:message key="Course.name" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="courseRank"><fmt:message key="Course.Rank"/> </label>
                            <div class="col-xs-8" data-tip="courseRank">
                                <input type="text" id="courseRank" name="courseRank"
                                       placeholder="<fmt:message key="Course.Rank" /> " class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="courseRating"><fmt:message key="Course.rating"/> </label>
                            <div class="col-xs-8" data-tip="courseRating">
                                <input type="text" id="courseRating" name="courseRating"
                                       placeholder="<fmt:message key="Course.rating" />" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Add"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=ADD_TRAINER">
                    <h2 class="text-center"><fmt:message key="Trainer.adding"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="directionID_trainer"><fmt:message key="Direction.ID"/></label>
                            <div class="col-xs-8" data-tip="directionID ">
                                <input type="number" id="directionID_trainer" name="directionID_trainer"
                                       placeholder="<fmt:message key="Direction.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstName"><fmt:message key="First.name"/></label>
                            <div class="col-xs-8" data-tip="firstName ">
                                <input type="text" id="firstName" name="firstName"
                                       placeholder="<fmt:message key="First.name" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="secondName"><fmt:message key="Second.name"/></label>
                            <div class="col-xs-8" data-tip="secondName">
                                <input type="text" id="secondName" name="secondName"
                                       placeholder="<fmt:message key="Second.name" /> " class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="trainerRank"><fmt:message key="Rank"/> </label>
                            <div class="col-xs-8" data-tip="trainerRank">
                                <input type="text" id="trainerRank" name="trainerRank"
                                       placeholder="<fmt:message key="Rank" /> " class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Add"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=ADD_DIRECTION">
                    <h2 class="text-center"><fmt:message key="Direction.adding"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="directionName"><fmt:message key="Direction.name"/></label>
                            <div class="col-xs-8" data-tip="directionName ">
                                <input type="text" id="directionName" name="directionName"
                                       placeholder="<fmt:message key="Direction.name" />"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Add"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-lg-offset-3">
                    <form class="form-horizontal" role="form" method="post" action="controller?command=DELETE_COURSE">
                        <h2 class="text-center"><fmt:message key="Course.deleting"/></h2>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <label for="courseID"><fmt:message key="Course.ID"/></label>
                                <div class="col-xs-8" data-tip="courseID ">
                                    <input type="number" id="courseID" name="courseID"
                                           placeholder="<fmt:message key="Course.ID" />"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 col-sm-offset-4">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="submit" class="btn btn-default btn-block"><fmt:message
                                                key="Delete"/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-lg-offset-3">
                    <form class="form-horizontal" role="form" method="post" action="controller?command=DELETE_TRAINER">
                        <h2 class="text-center"><fmt:message key="Trainer.deleting"/></h2>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <label for="trainerID"><fmt:message key="Trainer.ID"/></label>
                                <div class="col-xs-8" data-tip="trainerID ">
                                    <input type="number" id="trainerID" name="trainerID"
                                           placeholder="<fmt:message key="Trainer.ID" />"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 col-sm-offset-4">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="submit" class="btn btn-default btn-block"><fmt:message
                                                key="Delete"/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-lg-offset-3">
                    <form class="form-horizontal" role="form" method="post"
                          action="controller?command=DELETE_DIRECTION">
                        <h2 class="text-center"><fmt:message key="Direction.deleting"/></h2>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <label for="directionID_delete"><fmt:message key="Direction.ID"/></label>
                                <div class="col-xs-8" data-tip="directionID_delete ">
                                    <input type="number" id="directionID_delete" name="directionID_delete"
                                           placeholder="<fmt:message key="Direction.ID" />"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 col-sm-offset-4">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="submit" class="btn btn-default btn-block"><fmt:message
                                                key="Delete"/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</c:if>
<ctg:footer/>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>