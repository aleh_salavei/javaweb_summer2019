<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 01.08.2019
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
%>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Users</title>
</head>
<body>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <h2>Users Info</h2>
    <form class="form-inline">
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_main"><fmt:message key="main"/></a>
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_groups"><fmt:message key="groups"/></a>
        <c:if test="${user.role == 'ADMIN'}">
            <a class="btn btn-outline-success" href="controller?command=to_users"><fmt:message key="users"/></a>
        </c:if>
        <c:if test="${user.role != 'GUEST'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_user"><fmt:message
                    key="Profile"/></a>
        </c:if>
        <%-- <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_training"><fmt:message key="Training" /></a>
         <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_error"><fmt:message key="Error" /></a>--%>
        <c:choose>
            <c:when test="${user.role == 'GUEST'}">
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_login"><fmt:message
                        key="Login"/></a>
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_registration"><fmt:message
                        key="Registration"/></a>
            </c:when>
            <c:when test="${user.role != 'GUEST'}">
                <button class="btn btn-sm btn-outline-secondary" type="submit" formaction="controller?command=logout"
                        formmethod="post"><fmt:message key="Logout"/>
                </button>
            </c:when>
        </c:choose>
        <a class="btn btn-default " href="controller?lang=en&command=language">ENG</a>
        <a class="btn btn-default" href="controller?lang=ru&command=language">RUS</a>
    </form>
</nav>
<c:if test="${user.role == 'ADMIN'}">

    <table class="table table-striped">
        <thead>
        <fmt:message key="Users"/>
        <tr>
            <th scope="col"><fmt:message key="User.ID"/></th>
            <th scope="col"><fmt:message key="First.name"/></th>
            <th scope="col"><fmt:message key="Second.name"/></th>
            <th scope="col"><fmt:message key="User.login"/></th>
            <th scope="col"><fmt:message key="User.password"/></th>
            <th scope="col"><fmt:message key="Email"/></th>
            <th scope="col"><fmt:message key="Role"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${users}">
            <tr>
                <th scope="row">${user.idUser}</th>
                <td>${user.firstName}</td>
                <td>${user.secondName}</td>
                <td>${user.login}</td>
                <td>${user.password}</td>
                <td>${user.email}</td>
                <td>${user.role}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <table class="table table-striped">
        <thead>
        <fmt:message key="subs"/>
        <tr>
            <th scope="col"><fmt:message key="User.ID"/></th>
            <th scope="col"><fmt:message key="First.name"/></th>
            <th scope="col"><fmt:message key="Second.name"/></th>
            <th scope="col"><fmt:message key="User.login"/></th>
            <th scope="col"><fmt:message key="User.password"/></th>
            <th scope="col"><fmt:message key="Email"/></th>
            <th scope="col"><fmt:message key="Role"/></th>
            <th scope="col"><fmt:message key="Subscription.ID"/></th>
            <th scope="col"><fmt:message key="Subscription.start"/></th>
            <th scope="col"><fmt:message key="Subscription.end"/></th>
            <th scope="col"><fmt:message key="Subscription.quantity"/></th>
            <th scope="col"><fmt:message key="Subscription.quantity.left"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="sub" items="${subs}">
            <tr>
                <th scope="row">${sub.user.idUser}</th>
                <td>${sub.user.firstName}</td>
                <td>${sub.user.secondName}</td>
                <td>${sub.user.login}</td>
                <td>${sub.user.password}</td>
                <td>${sub.user.email}</td>
                <td>${sub.user.role}</td>
                <td>${sub.idSubscription}</td>
                <td>${sub.startDate}</td>
                <td>${sub.endDate}</td>
                <td>${sub.trainingQuantity}</td>
                <td>${sub.trainingQuantityLeft}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=ADD_SUBSCRIPTION">
                    <h2 class="text-center"><fmt:message key="Subscription.adding"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="userID"><fmt:message key="User.ID"/></label>
                            <div class="col-xs-8" data-tip="userID ">
                                <input type="number" id="userID" name="userID"
                                       placeholder="<fmt:message key="User.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="startDate"><fmt:message key="Subscription.start"/></label>
                            <div class="col-xs-8" data-tip="startDate">
                                <input type="date" id="startDate" name="startDate"
                                       placeholder="<fmt:message key="Subscription.start" /> " class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="endDate"><fmt:message key="Subscription.end"/></label>
                            <div class="col-xs-8" data-tip="endDate">
                                <input type="date" id="endDate" name="endDate"
                                       placeholder="<fmt:message key="Subscription.end" />" class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="trainingQuantity"><fmt:message key="Subscription.quantity"/></label>
                            <div class="col-xs-8" data-tip="trainingQuantity">
                                <input type="number" id="trainingQuantity" name="trainingQuantity"
                                       placeholder="<fmt:message key="Subscription.quantity" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="trainingQuantityLeft"><fmt:message key="Subscription.quantity.left"/></label>
                            <div class="col-xs-8" data-tip="trainingQuantityLeft">
                                <input type="number" id="trainingQuantityLeft" name="trainingQuantityLeft"
                                       placeholder="<fmt:message key="Subscription.quantity.left" />"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Add"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=DELETE_SUBSCRIPTION">
                    <h2 class="text-center"><fmt:message key="Subscription.deleting"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="subscriptionID"><fmt:message key="Subscription.ID"/></label>
                            <div class="col-xs-8" data-tip="userID ">
                                <input type="number" id="subscriptionID" name="subscriptionID"
                                       placeholder="<fmt:message key="Subscription.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Delete"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</c:if>
<ctg:footer/>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
