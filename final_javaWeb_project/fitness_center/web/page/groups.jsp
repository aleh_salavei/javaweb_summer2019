<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 01.08.2019
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
%>
<html lang="loc">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Groups</title>
</head>
<body>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <h2>Groups Info</h2>
    <form class="form-inline">
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_main"><fmt:message key="main"/></a>
        <a class="btn btn-outline-success" href="controller?command=to_groups"><fmt:message key="groups"/></a>
        <c:if test="${user.role == 'ADMIN'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_users"><fmt:message
                    key="users"/></a>
        </c:if>
        <c:if test="${user.role != 'GUEST'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_user"><fmt:message
                    key="Profile"/></a>
        </c:if>
        <%--<a class="btn btn-sm btn-outline-secondary" href="controller?command=to_training"><fmt:message key="Training" /></a>
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_error"><fmt:message key="Error" /></a>--%>
        <c:choose>
            <c:when test="${user.role == 'GUEST'}">
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_login"><fmt:message
                        key="Login"/></a>
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_registration"><fmt:message
                        key="Registration"/></a>
            </c:when>
            <c:when test="${user.role != 'GUEST'}">
                <button class="btn btn-sm btn-outline-secondary" type="submit" formaction="controller?command=logout"
                        formmethod="post"><fmt:message key="Logout"/>
                </button>
            </c:when>
        </c:choose>
        <a class="btn btn-default " href="controller?lang=en&command=language">ENG</a>
        <a class="btn btn-default" href="controller?lang=ru&command=language">RUS</a>
    </form>
</nav>

<table class="table table-striped">
    <thead>
    <fmt:message key="Groups.info"/>
    <tr>
        <th scope="col"><fmt:message key="Group.ID"/></th>
        <th scope="col"><fmt:message key="Group.trainer.name"/></th>
        <th scope="col"><fmt:message key="Group.day"/></th>
        <th scope="col"><fmt:message key="Group.time"/></th>
        <th scope="col"><fmt:message key="Group.quantity"/></th>
        <th scope="col"><fmt:message key="Group.quantity.available"/></th>
        <th scope="col"><fmt:message key="Course.name"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="group" items="${groups}">
        <tr>
            <th scope="row"><a href="controller?command=to_training&idGroup=${group.idGroup}">${group.idGroup}</a></th>
            <td>${group.trainer.firstName}</td>
            <td>${group.day}</td>
            <td>${group.time}</td>
            <td>${group.groupCapacity}</td>
            <td>${group.groupFreeCapacity}</td>
            <td>${group.course.name}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<c:if test="${user.role == 'ADMIN'}">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller?command=ADD_GROUP">
                    <h2 class="text-center"><fmt:message key="Group.adding"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="trainerID"><fmt:message key="Trainer.ID"/></label>
                            <div class="col-xs-8" data-tip="trainerID ">
                                <input type="number" id="trainerID" name="trainerID"
                                       placeholder="<fmt:message key="Trainer.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="day"><fmt:message key="Group.day"/></label>
                            <div class="col-xs-8" data-tip="day ">
                                <input type="date" id="day" name="day" placeholder="<fmt:message key="Group.day" />"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="time"><fmt:message key="Group.time"/> </label>
                            <div class="col-xs-8" data-tip="time">
                                <input type="time" id="time" name="time"
                                       placeholder="<fmt:message key="Group.time" /> " class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groupCapacity"><fmt:message key="Group.quantity"/> </label>
                            <div class="col-xs-8" data-tip="groupCapacity">
                                <input type="number" id="groupCapacity" name="groupCapacity"
                                       placeholder="<fmt:message key="Group.quantity" /> " class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="courseID"><fmt:message key="Course.ID"/></label>
                            <div class="col-xs-8" data-tip="courseID ">
                                <input type="number" id="courseID" name="courseID"
                                       placeholder="<fmt:message key="Course.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Add"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post"
                      action="controller?command=DELETE_GROUP">
                    <h2 class="text-center"><fmt:message key="Group.deleting"/></h2>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="groupID_delete"><fmt:message key="Group.ID"/></label>
                            <div class="col-xs-8" data-tip="groupID_delete ">
                                <input type="number" id="groupID_delete" name="groupID_delete"
                                       placeholder="<fmt:message key="Group.ID" />"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message
                                            key="Delete"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</c:if>
<ctg:footer/>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>