<%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 01.08.2019
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
%>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>User</title>
</head>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <h2>User Info</h2>
    <form class="form-inline">
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_main"><fmt:message key="main"/></a>
        <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_groups"><fmt:message key="groups"/></a>
        <c:if test="${user.role == 'ADMIN'}">
            <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_users"><fmt:message
                    key="users"/></a>
        </c:if>
        <c:if test="${user.role != 'GUEST'}">
            <a class="btn btn-outline-success" href="controller?command=to_user"><fmt:message key="Profile"/></a>
        </c:if>
        <%-- <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_training"><fmt:message key="Training" /></a>
         <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_error"><fmt:message key="Error" /></a>--%>
        <c:choose>
            <c:when test="${user.role == 'GUEST'}">
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_login"><fmt:message
                        key="Login"/></a>
                <a class="btn btn-sm btn-outline-secondary" href="controller?command=to_registration"><fmt:message
                        key="Registration"/></a>
            </c:when>
            <c:when test="${user.role != 'GUEST'}">
                <button class="btn btn-sm btn-outline-secondary" type="submit" formaction="controller?command=logout"
                        formmethod="post"><fmt:message key="Logout"/>
                </button>
            </c:when>
        </c:choose>
        <a class="btn btn-default " href="controller?lang=en&command=language">ENG</a>
        <a class="btn btn-default" href="controller?lang=ru&command=language">RUS</a>
    </form>
</nav>
<table class="table table-striped">
    <thead>
    <fmt:message key="user.info"/>
    <tr>
        <th scope="col"><fmt:message key="First.name"/></th>
        <th scope="col"><fmt:message key="Second.name"/></th>
        <th scope="col"><fmt:message key="User.login"/></th>
        <th scope="col"><fmt:message key="User.password"/></th>
        <th scope="col"><fmt:message key="Email"/></th>
        <th scope="col"><fmt:message key="Role"/></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>${user.firstName}</td>
        <td>${user.secondName}</td>
        <td>${user.login}</td>
        <td>${user.password}</td>
        <td>${user.email}</td>
        <td>${user.role}</td>
    </tr>
    </tbody>
</table>
<c:if test="${user.role == 'USER'}">
    <table class="table table-striped">
        <thead>
        <fmt:message key="user.subs.info"/>
        <tr>
            <th scope="col"><fmt:message key="Subscription.ID"/></th>
            <th scope="col"><fmt:message key="Subscription.start"/></th>
            <th scope="col"><fmt:message key="Subscription.end"/></th>
            <th scope="col"><fmt:message key="Subscription.quantity"/></th>
            <th scope="col"><fmt:message key="Subscription.quantity.left"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="subscription" items="${subscriptions}">
            <tr>
                <td>${subscription.idSubscription}</td>
                <td>${subscription.startDate}</td>
                <td>${subscription.endDate}</td>
                <td>${subscription.trainingQuantity}</td>
                <td>${subscription.trainingQuantityLeft}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <table class="table table-striped">
        <thead>
        <fmt:message key="user.trainings.info"/>
        <tr>
            <th scope="col"><fmt:message key="Training.ID"/></th>
            <th scope="col"><fmt:message key="Group.day"/></th>
            <th scope="col"><fmt:message key="Group.time"/></th>
            <th scope="col"><fmt:message key="Course.name"/></th>
            <th scope="col"><fmt:message key="Subscription.ID"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="training" items="${trainings}">
            <tr>
                <td>
                    <a href="controller?command=to_training&idGroup=${training.group.idGroup}">${training.idTraining}</a>
                </td>
                <td>${training.group.day}</td>
                <td>${training.group.time}</td>
                <td>${training.group.course.name}</td>
                <td>${training.subscription.idSubscription}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
<ctg:footer/>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
