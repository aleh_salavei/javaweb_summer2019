-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema fitness_center_lite
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema fitness_center_lite
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `fitness_center_lite` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `fitness_center_lite` ;

-- -----------------------------------------------------
-- Table `fitness_center_lite`.`directions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`directions` (
  `idDirection` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID number of direction',
  `name` VARCHAR(255) NOT NULL COMMENT 'name of direction',
  PRIMARY KEY (`idDirection`),
  UNIQUE INDEX `idDirection_UNIQUE` (`idDirection` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'this table have information about all directions of fitness center\'s trainings. this table connect concrete trainers with concrete types of trainings that they can teach according their specialization';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`courses` (
  `idCourse` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID number of course',
  `name` VARCHAR(255) NOT NULL COMMENT 'name of course',
  `rank` VARCHAR(255) NOT NULL COMMENT 'rank of course (like \"proffessional\", \"easy\", etc.)',
  `raiting` FLOAT UNSIGNED NOT NULL COMMENT 'rating of course according to comments of trainees',
  `idDirection_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of direction of this course',
  PRIMARY KEY (`idCourse`),
  UNIQUE INDEX `idCourse_UNIQUE` (`idCourse` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `idDirection_FK_idx` (`idDirection_FK` ASC) VISIBLE,
  CONSTRAINT `idDirection_FK`
    FOREIGN KEY (`idDirection_FK`)
    REFERENCES `fitness_center_lite`.`directions` (`idDirection`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'this table contains information about all concrete types of trainings in the fitness center. every type belongs to a concrete direction of trainings';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`trainers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`trainers` (
  `idTrainers` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'unique ID number of each coach that works at finess center',
  `firstName` VARCHAR(255) NOT NULL COMMENT 'first name of coach',
  `secondName` VARCHAR(255) NOT NULL COMMENT 'second name of coach',
  `idDirection_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of direction which this coach teach',
  `rank` VARCHAR(255) NOT NULL COMMENT 'the rank of coach (1 category, master of sports, etc.)',
  PRIMARY KEY (`idTrainers`),
  UNIQUE INDEX `idTrainers_UNIQUE` (`idTrainers` ASC) VISIBLE,
  INDEX `idDirection_FK_idx` (`idDirection_FK` ASC) VISIBLE,
  CONSTRAINT `idDirection_FK_tr_dir`
    FOREIGN KEY (`idDirection_FK`)
    REFERENCES `fitness_center_lite`.`directions` (`idDirection`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'the table with information aboul all trainers that works at fitness center. every trainer have a specialization of his work (direction)';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`groups` (
  `idGroup` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID number of group',
  `idTrainer_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of trainer in this group',
  `day` DATE NOT NULL COMMENT 'day of holding of this group',
  `time` TIME NOT NULL COMMENT 'time of holding of this group',
  `groupCapacity` INT UNSIGNED NOT NULL COMMENT 'total count of trainees that can go in this group',
  `groupFreeCapacity` INT UNSIGNED NOT NULL COMMENT 'count of free places in concrete group',
  `idCourse_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of curse of this group',
  PRIMARY KEY (`idGroup`),
  UNIQUE INDEX `idGroups_UNIQUE` (`idGroup` ASC) VISIBLE,
  INDEX `idTrainer_FK_idx` (`idTrainer_FK` ASC) VISIBLE,
  INDEX `idCourse_FK_gr_cors_idx` (`idCourse_FK` ASC) VISIBLE,
  CONSTRAINT `idCourse_FK_gr_cors`
    FOREIGN KEY (`idCourse_FK`)
    REFERENCES `fitness_center_lite`.`courses` (`idCourse`),
  CONSTRAINT `idTrainer_FK_gr_tr`
    FOREIGN KEY (`idTrainer_FK`)
    REFERENCES `fitness_center_lite`.`trainers` (`idTrainers`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'the table with information of all training groups that are represented in the fitness center and are available to trainees. every group have a concrete course and trainer';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`trainees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`trainees` (
  `idTrainee` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID number of client',
  `firstName` VARCHAR(255) NOT NULL COMMENT 'first name of client',
  `secondName` VARCHAR(255) NOT NULL COMMENT 'second name of client',
  `login` VARCHAR(255) NOT NULL COMMENT 'login for registration in web to registrating and canceling trainings',
  `password` VARCHAR(255) NOT NULL COMMENT 'password for registration',
  `email` VARCHAR(255) NOT NULL COMMENT 'contact info: e-mail',
  PRIMARY KEY (`idTrainee`),
  UNIQUE INDEX `idTrainee_UNIQUE` (`idTrainee` ASC) VISIBLE,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'the table with information about all clients who ever was at this fitness center';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`subscriptions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`subscriptions` (
  `idSubscription` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID number of subscription',
  `idTrainee_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of trainee who use this subscription',
  `startDate` DATE NOT NULL COMMENT 'date of registration of this subscription',
  `endDate` DATE NOT NULL COMMENT 'date of ending of this subscription (if this date is coming before trainee used all his available trainings this trainings are burned)',
  `trainingQuantity` INT UNSIGNED NOT NULL COMMENT 'the total quantity of available trainings at start of subscription',
  `trainingQuantityLeft` INT UNSIGNED NOT NULL COMMENT 'quantity of available trainings left at the current moment',
  PRIMARY KEY (`idSubscription`),
  UNIQUE INDEX `idSubscription_UNIQUE` (`idSubscription` ASC) VISIBLE,
  INDEX `idTrainee_FK_sub_trainee_idx` (`idTrainee_FK` ASC) VISIBLE,
  CONSTRAINT `idTrainee_FK_sub_trainee`
    FOREIGN KEY (`idTrainee_FK`)
    REFERENCES `fitness_center_lite`.`trainees` (`idTrainee`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'the table that contains all subscriptions since the begininng of fitness-center, for that reason there can be a many different subscriptions according to one trainee.';


-- -----------------------------------------------------
-- Table `fitness_center_lite`.`trainings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fitness_center_lite`.`trainings` (
  `idTraining` INT NOT NULL AUTO_INCREMENT COMMENT 'ID number of training',
  `idSubscription_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of subscription that take a part in this training',
  `idGroup_FK` INT UNSIGNED NOT NULL COMMENT 'ID number of group of current training',
  PRIMARY KEY (`idTraining`),
  UNIQUE INDEX `idTraining_UNIQUE` (`idTraining` ASC) VISIBLE,
  INDEX `idSubscription_FK_training_sub_idx` (`idSubscription_FK` ASC) VISIBLE,
  INDEX `idGroup_FK_training_gr_idx` (`idGroup_FK` ASC) VISIBLE,
  CONSTRAINT `idGroup_FK_training_gr`
    FOREIGN KEY (`idGroup_FK`)
    REFERENCES `fitness_center_lite`.`groups` (`idGroup`),
  CONSTRAINT `idSubscription_FK_training_sub`
    FOREIGN KEY (`idSubscription_FK`)
    REFERENCES `fitness_center_lite`.`subscriptions` (`idSubscription`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'this table is for \"many to many\" connecting of subscriptions and groups. it\'s made because one group consists of many trainees and every trainee can go into few groups with one subscription.';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
