package by.epam.fitness.activity.dao;

import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Role;
import by.epam.fitness.entity.table.User;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class DAOUserTest {
    private User user;
    private DAOUser daoUser;

    @BeforeClass
    public void setUp() {
        daoUser = new DAOUser();
        user = new User("TestName", "TestSecName", "testLogin",
                "testPass", "test@mail.ru", Role.USER);
    }

    @AfterClass
    public void tearDown() {
        daoUser = null;
        user = null;
    }

    @Test(dependsOnMethods = {"testAdd"})
    public void testGet() throws DAOException {
        User backUpUser = daoUser.defineUser("testLogin", "testPass");
        user.setIdUser(backUpUser.getIdUser());
        assertEquals(user, daoUser.get(backUpUser.getIdUser()));
    }

    @Test(dependsOnMethods = {"testAdd"})
    public void testDefineUser() throws DAOException {
        User backUpUser = daoUser.defineUser("testLogin", "testPass");
        user.setIdUser(backUpUser.getIdUser());
        assertEquals(user, backUpUser);
    }

    @Test
    public void testAdd() throws DAOException {
        daoUser.add(user);
        assertNotNull(daoUser.defineUser("testLogin", "testPass"));
    }

    @Test(dependsOnMethods = {"testAdd", "testDefineUser"})
    public void testDelete() throws DAOException {
        User backUpUser = daoUser.defineUser("testLogin", "testPass");
        daoUser.delete(backUpUser.getIdUser());
        assertNull(daoUser.defineUser("testLogin", "testPass"));
    }
}