package by.epam.fitness.activity.connection;

import by.epam.fitness.entity.exception.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConnectionPool {

    private static Logger logger = LogManager.getLogger(ConnectionPool.class);
    private final static String URL = "jdbc:mysql://localhost:3306/fitness_center_lite?serverTimezone=" + TimeZone.getDefault().getID();
    private final static Properties PROP = new Properties();
    private final static int POOL_SIZE = 30;
    private final static int TIMEOUT = 3;
    private ArrayBlockingQueue<ProxyConnection> pool;

    private static class InnerPool {
        private static ConnectionPool instance = new ConnectionPool();
    }

    private ConnectionPool() {
        PROP.put("user", "root");
        PROP.put("password", "baltacom");
        PROP.put("autoReconnect", "true");
        PROP.put("characterEncoding", "UTF-8");
        PROP.put("useUnicode", "true");
        pool = new ArrayBlockingQueue<>(POOL_SIZE);
        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                Connection connection = DriverManager.getConnection(URL, PROP);
                pool.offer(new ProxyConnection(connection));
                logger.info("connection #" + i + " created");
            } catch (SQLException e) {
                logger.error("can't create connection #" + i);
            }
        }
    }

    public static ConnectionPool getInstance() {
        return InnerPool.instance;
    }

    public ProxyConnection getConnection() throws DBException {
        ProxyConnection connection;
        try {
            connection = pool.poll(TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new DBException(e);
        }
        return connection;
    }

    public void returnConnection(ProxyConnection connection) {
        if (connection != null) {
            pool.offer(connection);
        }
    }

    public void closeConnections() {
        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                pool.take().closeRealConnection();
            } catch (SQLException | InterruptedException e) {
                logger.error(e);
            }
        }
    }
}

