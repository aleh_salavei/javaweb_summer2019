package by.epam.fitness.activity.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Random;

@WebFilter(urlPatterns = {"/controller"})
public class F5ProtectionFilter implements Filter {

    private static Random rnd = new Random();
    private String check;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        String oldCheck = request.getParameter("check");
        String lastCommand = (String) session.getAttribute("lastCommand");
        if (oldCheck != null && !check.equals(oldCheck)) {
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/controller?command=" + lastCommand);
            dispatcher.forward(request, response);
            return;
        }
        check = String.valueOf(rnd.nextLong());
        session.setAttribute("check", check);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}


