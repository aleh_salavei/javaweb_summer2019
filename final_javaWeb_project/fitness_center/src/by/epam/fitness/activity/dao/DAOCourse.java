package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOCourse implements IDAO<Course> {

    @Override
    public List<Course> getAll() throws DAOException {
        List<Course> courses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses LEFT JOIN " +
                     "directions ON idDirection_FK = idDirection ORDER BY directions.name")) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                courses.add(createCourse(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all courses taking");
        }
        return courses;
    }

    @Override
    public Course get(int id) throws DAOException {
        Course course = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses WHERE idCourse=?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                course = createCourse(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during course taking", e);
        }
        return course;
    }

    public List<Course> getAllByDirection(String directionName) throws DAOException {
        List<Course> courses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses RIGHT JOIN " +
                     "directions ON idDirection_FK = idDirection WHERE directions.name = ? ORDER BY course.rank")) {
            statement.setString(1, directionName);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                courses.add(createCourse(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all courses taking");
        }
        return courses;
    }

    public List<Course> getAllByRank(String rank) throws DAOException {
        List<Course> courses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses RIGHT JOIN " +
                     "directions ON idDirection_FK = idDirection WHERE courses.rank = ? ORDER BY directions.name")) {
            statement.setString(1, rank);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                courses.add(createCourse(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all courses taking");
        }
        return courses;
    }

    @Override
    public void add(Course course) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO courses " +
                     "(courses.name, courses.rank, courses.raiting, courses.idDirection_FK) " +
                     "VALUES (?, ?, ?, ?)")) {
            statement.setString(1, course.getName());
            statement.setString(2, course.getRank());
            statement.setFloat(3, course.getRating());
            statement.setInt(4, course.getDirection().getIdDirection());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during course adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM courses WHERE idCourse=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during course deleting", e);
        }
    }

    @Override
    public void update(int id, Course course) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE courses SET name=?, " +
                     "courses.rank=?, raiting=?, idDirection_FK=? WHERE idSubscription=?")) {
            statement.setString(1, course.getName());
            statement.setString(2, course.getRank());
            statement.setFloat(3, course.getRating());
            statement.setInt(4, course.getDirection().getIdDirection());
            statement.setInt(5, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during course updating", e);
        }
    }

    private Course createCourse(ResultSet resultSet) throws DAOException {
        Course course = null;
        DAODirection daoDirection = new DAODirection();
        try {
            course = new Course(daoDirection.get(resultSet.getInt("idDirection_FK")),
                    resultSet.getString("name"),
                    resultSet.getFloat("raiting"),
                    resultSet.getString("rank"));
            course.setIdCourse(resultSet.getInt("idCourse"));
        } catch (SQLException | DAOException e) {
            throw new DAOException("Invalid database operation occurred during course creating", e);
        }
        return course;
    }
}
