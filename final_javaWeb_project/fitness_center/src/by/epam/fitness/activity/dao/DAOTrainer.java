package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Trainer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOTrainer implements IDAO<Trainer> {
    @Override
    public List<Trainer> getAll() throws DAOException {
        List<Trainer> userList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainers LEFT JOIN " +
                     "directions ON idDirection_FK = idDirection ORDER BY directions.name")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userList.add(createTrainer(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all trainers taking", e);
        }
        return userList;
    }

    @Override
    public Trainer get(int id) throws DAOException {
        Trainer trainer = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainers WHERE idTrainers = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trainer = createTrainer(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during trainer taking", e);
        }
        return trainer;
    }

    @Override
    public void add(Trainer trainer) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO trainers " +
                     "(firstName, secondName, idDirection_FK, trainers.rank) VALUES (?, ?, ?, ?)")) {
            statement.setString(1, trainer.getFirstName());
            statement.setString(2, trainer.getSecondName());
            statement.setInt(3, trainer.getDirection().getIdDirection());
            statement.setString(4, trainer.getRank());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during trainer adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM trainers WHERE idTrainers=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during trainer deleting", e);
        }
    }

    @Override
    public void update(int id, Trainer trainer) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE trainers SET firstName=?, " +
                     "secondName=?, idDirection_FK=?, rank=? WHERE idSubscription=?")) {
            statement.setString(1, trainer.getFirstName());
            statement.setString(2, trainer.getSecondName());
            statement.setInt(3, trainer.getDirection().getIdDirection());
            statement.setString(4, trainer.getRank());
            statement.setInt(5, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during trainer updating", e);
        }
    }

    private Trainer createTrainer(ResultSet resultSet) throws DAOException {
        Trainer trainer = null;
        DAODirection daoDirection = new DAODirection();
        try {
            trainer = new Trainer(resultSet.getString("firstName"),
                    resultSet.getString("secondName"),
                    daoDirection.get(resultSet.getInt("idDirection_FK")),
                    resultSet.getString("rank"));
            trainer.setIdTrainer(resultSet.getInt("idTrainers"));
        } catch (SQLException | DAOException e) {
            throw new DAOException("Invalid database operation occurred during trainer creating", e);
        }
        return trainer;
    }
}
