package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOGroup implements IDAO<Group> {

    @Override
    public List<Group> getAll() throws DAOException {
        List<Group> groupList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.groups " +
                     "JOIN trainers ON fitness_center_lite.groups.idTrainer_FK = trainers.idTrainers " +
                     "JOIN courses ON fitness_center_lite.groups.idCourse_FK = courses.idCourse")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                groupList.add(createGroup(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all groups taking", e);
        }
        return groupList;
    }

    @Override
    public Group get(int id) throws DAOException {
        Group group = new Group();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.groups " +
                     "JOIN trainers ON fitness_center_lite.groups.idTrainer_FK = trainers.idTrainers " +
                     "JOIN courses ON fitness_center_lite.groups.idCourse_FK = courses.idCourse " +
                     "WHERE fitness_center_lite.groups.idGroup = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                group = createGroup(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during group taking", e);
        }
        return group;
    }

    public List<Group> getAllByCourse(String courseName) throws DAOException {
        List<Group> groupList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.groups " +
                     "JOIN trainers ON fitness_center_lite.groups.idTrainer_FK = trainers.idTrainers " +
                     "JOIN courses ON fitness_center_lite.groups.idCourse_FK = courses.idCourse " +
                     "WHERE courses.name = ?")) {
            statement.setString(1, courseName);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                groupList.add(createGroup(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during groups taking", e);
        }
        return groupList;
    }

    public List<Group> getAllByTrainer(int id) throws DAOException {
        List<Group> groupList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.groups " +
                     "JOIN trainers ON fitness_center_lite.groups.idTrainer_FK = trainers.idTrainers " +
                     "JOIN courses ON fitness_center_lite.groups.idCourse_FK = courses.idCourse " +
                     "WHERE fitness_center_lite.groups.idTrainer_FK = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                groupList.add(createGroup(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during groups taking", e);
        }
        return groupList;
    }

    @Override
    public void add(Group group) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO fitness_center_lite.groups " +
                     "(idTrainer_FK, fitness_center_lite.groups.day, fitness_center_lite.groups.time, groupCapacity, groupFreeCapacity, idCourse_FK) VALUES (?, ?, ?, ?, ?, ?)")) {
            System.out.println(group);
            statement.setInt(1, group.getTrainer().getIdTrainer());
            statement.setDate(2, group.getDay());
            statement.setTime(3, group.getTime());
            statement.setInt(4, group.getGroupCapacity());
            statement.setInt(5, group.getGroupFreeCapacity());
            statement.setInt(6, group.getCourse().getIdCourse());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during group adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM fitness_center_lite.groups " +
                     "WHERE idGroup=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during group deleting", e);
        }
    }

    @Override
    public void update(int id, Group group) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE fitness_center_lite.groups SET idTrainer_FK=?, " +
                     "fitness_center_lite.groups.day=?, fitness_center_lite.groups.time=?, groupFreeCapacity=? WHERE idGroup=?")) {
            statement.setInt(1, group.getTrainer().getIdTrainer());
            statement.setDate(2, group.getDay());
            statement.setTime(3, group.getTime());
            statement.setInt(4, group.getGroupFreeCapacity());
            statement.setInt(5, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during group updating", e);
        }
    }

    private Group createGroup(ResultSet resultSet) throws DAOException {
        Group group = null;
        DAOTrainer daoTrainer = new DAOTrainer();
        DAOCourse daoCourse = new DAOCourse();
        try {
            group = new Group(daoTrainer.get(resultSet.getInt("idTrainer_FK")),
                    resultSet.getDate("day"),
                    resultSet.getTime("time"),
                    resultSet.getInt("groupCapacity"),
                    resultSet.getInt("groupFreeCapacity"),
                    daoCourse.get(resultSet.getInt("idCourse_FK")));
            group.setIdGroup(resultSet.getInt("idGroup"));
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during group creating", e);
        }
        return group;
    }
}
