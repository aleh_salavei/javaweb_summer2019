package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Training;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOTraining implements IDAO<Training> {
    @Override
    public List<Training> getAll() throws DAOException {
        List<Training> trainingList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainings;")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trainingList.add(createTraining(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all trainings taking", e);
        }
        return trainingList;
    }

    @Override
    public Training get(int id) throws DAOException {
        Training training = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainings " +
                     "WHERE idTraining=?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                training = createTraining(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during training taking", e);
        }
        return training;
    }

    public List<Training> getAllByUserID(int id) throws DAOException {
        List<Training> trainingList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainings LEFT JOIN subscriptions ON " +
                     "idSubscription_FK = idSubscription WHERE subscriptions.idTrainee_FK = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trainingList.add(createTraining(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user trainings taking", e);
        }
        return trainingList;
    }

    public Training defineTraining(int groupID, int subscriptionID) throws DAOException {
        Training training = null;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM trainings " +
                     "WHERE idGroup_FK=? AND idSubscription_FK=?")) {
            statement.setInt(1, groupID);
            statement.setInt(2, subscriptionID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                training = createTraining(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user defining", e);
        }
        return training;
    }

    @Override
    public void add(Training training) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO trainings " +
                     "(idSubscription_FK, idGroup_FK) VALUES (?, ?)")) {
            statement.setInt(1, training.getSubscription().getIdSubscription());
            statement.setInt(2, training.getGroup().getIdGroup());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during training adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM trainings " +
                     "WHERE idTraining=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during training deleting", e);
        }
    }

    @Override
    public void update(int id, Training training) {

    }

    private Training createTraining(ResultSet resultSet) throws DAOException {
        Training training = null;
        DAOSubscription daoSubscription = new DAOSubscription();
        DAOGroup daoGroup = new DAOGroup();
        try {
            training = new Training(daoSubscription.get(resultSet.getInt("idSubscription_FK")),
                    daoGroup.get(resultSet.getInt("idGroup_FK")));
            training.setIdTraining(resultSet.getInt("idTraining"));
        } catch (SQLException | DAOException e) {
            throw new DAOException("Invalid database operation occurred during training creating", e);
        }
        return training;
    }
}
