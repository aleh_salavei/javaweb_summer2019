package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Subscription;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOSubscription implements IDAO<Subscription> {

    @Override
    public List<Subscription> getAll() throws DAOException {
        List<Subscription> subList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.users " +
                     "RIGHT JOIN fitness_center_lite.subscriptions ON users.idUser=subscriptions.idTrainee_FK")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                subList.add(createSubscription(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all subscriptions taking", e);
        }
        return subList;
    }

    @Override
    public Subscription get(int id) throws DAOException {
        Subscription sub = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM  subscriptions " +
                     "WHERE idSubscription = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                sub = createSubscription(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscription taking", e);
        }
        return sub;
    }

    public List<Subscription> getAllByUser(int id) throws DAOException {
        List<Subscription> subList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.users " +
                     "RIGHT JOIN fitness_center_lite.subscriptions ON users.idUser=subscriptions.idTrainee_FK " +
                     "WHERE idUser = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                subList.add(createSubscription(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscriptions taking", e);
        }
        return subList;
    }

    @Override
    public void add(Subscription subscription) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO fitness_center_lite.subscriptions " +
                     "(idTrainee_FK, startDate, endDate, trainingQuantity, trainingQuantityLeft) " +
                     "VALUES (?, ?, ?, ?, ?)")) {
            statement.setInt(1, subscription.getUser().getIdUser());
            statement.setDate(2, subscription.getStartDate());
            statement.setDate(3, subscription.getEndDate());
            statement.setInt(4, subscription.getTrainingQuantity());
            statement.setInt(5, subscription.getTrainingQuantityLeft());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscription adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM subscriptions WHERE idSubscription=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscription deleting", e);
        }
    }

    @Override
    public void update(int id, Subscription subscription) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE subscriptions SET startDate=?, " +
                     "endDate=?, trainingQuantity=?, trainingQuantityLeft=? WHERE idSubscription=?")) {
            statement.setDate(1, subscription.getStartDate());
            statement.setDate(2, subscription.getEndDate());
            statement.setInt(3, subscription.getTrainingQuantity());
            statement.setInt(4, subscription.getTrainingQuantityLeft());
            statement.setInt(5, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscription updating", e);
        }
    }

    private Subscription createSubscription(ResultSet resultSet) throws DAOException {
        Subscription subscription = null;
        DAOUser daoUser = new DAOUser();
        try {
            subscription = new Subscription(daoUser.get(resultSet.getInt("idTrainee_FK")),
                    resultSet.getDate("startDate"),
                    resultSet.getDate("endDate"),
                    resultSet.getInt("trainingQuantity"),
                    resultSet.getInt("trainingQuantityLeft"));
            subscription.setIdSubscription(resultSet.getInt("idSubscription"));
        } catch (SQLException | DAOException e) {
            throw new DAOException("Invalid database operation occurred during subscription creating", e);
        }
        return subscription;
    }
}
