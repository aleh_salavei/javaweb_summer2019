package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Role;
import by.epam.fitness.entity.table.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleh Salavei
 * <p>
 * The {@code DAOUser} class implements all the necessary methods
 * that were implemented from {@code IDAO} interface and using
 * for manipulations with user's data in database.
 */

public class DAOUser implements IDAO<User> {

    /**
     * Get all users from database
     *
     * @return list of instances {@code User}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    @Override
    public List<User> getAll() throws DAOException {
        List<User> userList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.users ")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userList.add(createUser(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all users taking", e);
        }
        return userList;
    }

    /**
     * Get user from database by his id
     *
     * @param id is user's ID number
     * @return instance {@code User} if he exists
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    @Override
    public User get(int id) throws DAOException {
        User user = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE users.idUser=?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = createUser(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
        return user;
    }

    /**
     * Get user from database by his subscription
     *
     * @param id is ID number of instance {@code Subscription}
     * @return instance {@code User} if he exists
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public User getBySubscription(int id) throws DAOException {
        User user = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users JOIN subscriptions " +
                     "ON users.idUser = subscriptions.idTrainee_FK WHERE subscriptions.idSubscription = ?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.first()) {
                user = createUser(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
        return user;
    }

    /**
     * Defines user by user login and password
     *
     * @param login    is user's login string
     * @param password is user password string
     * @return instance of{@code User} with such {@code login} and
     * {@code password} if he exists
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public User defineUser(String login, String password) throws DAOException {
        User user = null;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM fitness_center_lite.users " +
                     "WHERE login=? AND password=?")) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = createUser(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user defining", e);
        }
        return user;
    }

    /**
     * Adds instance of {@code User} to database
     *
     * @param user is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    @Override
    public void add(User user) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO fitness_center_lite.users " +
                     "(firstName, secondName, login, password, email, role) VALUES (?, ?, ?, ?, ?, ?)")) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getSecondName());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getEmail());
            statement.setString(6, String.valueOf(user.getRole().getIdRole()));
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        }
    }

    /**
     * Deletes user from database by user's identification number
     *
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM fitness_center_lite.users " +
                     "WHERE idUser=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user deleting", e);
        }
    }

    /**
     * Updates instance of {@code User} in database
     *
     * @param id   is ID number of old instance
     * @param user is new instance
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    @Override
    public void update(int id, User user) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE users SET firstName=?, " +
                     "secondName=?, password=?, email=? WHERE idSubscription=?")) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getSecondName());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getEmail());
            statement.setInt(5, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during subscription updating", e);
        }
    }

    /**
     * Creates instance of {@code User} from resultSet
     *
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code User}
     * @throws DAOException if there are any troubles with query execution.
     */
    private User createUser(ResultSet resultSet) throws DAOException {
        User user = null;
        try {
            user = new User(resultSet.getString("firstName"),
                    resultSet.getString("secondName"),
                    resultSet.getString("login"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    Role.valueOf((resultSet.getString("role")).toUpperCase()));
            user.setIdUser(resultSet.getInt("idUser"));
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user creating", e);
        }
        return user;
    }
}
