package by.epam.fitness.activity.dao;

import by.epam.fitness.activity.connection.ConnectionPool;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.exception.DBException;
import by.epam.fitness.entity.table.Direction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAODirection implements IDAO<Direction> {

    @Override
    public List<Direction> getAll() throws DAOException {
        List<Direction> directionList = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM directions ")) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                directionList.add(createDirection(resultSet));
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all directions taking", e);
        }
        return directionList;
    }

    public Direction get(int id) throws DAOException {
        Direction direction = null;
        ResultSet resultSet;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM directions " +
                     "WHERE idDirection=?")) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                direction = createDirection(resultSet);
            }
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during direction taking", e);
        }
        return direction;
    }

    @Override
    public void add(Direction direction) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO directions " +
                     "(directions.name) VALUES (?)")) {
            statement.setString(1, direction.getName());
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during direction adding", e);
        }
    }

    @Override
    public void delete(int id) throws DAOException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM directions WHERE idDirection=?")) {
            statement.setInt(1, id);
            statement.execute();
        } catch (DBException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during direction deleting", e);
        }
    }

    @Override
    public void update(int id, Direction direction) {

    }

    private Direction createDirection(ResultSet resultSet) throws DAOException {
        Direction direction = null;
        try {
            direction = new Direction(resultSet.getString("name"));
            direction.setIdDirection(resultSet.getInt("idDirection"));
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during direction creating", e);
        }
        return direction;
    }
}
