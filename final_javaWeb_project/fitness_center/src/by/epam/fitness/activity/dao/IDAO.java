package by.epam.fitness.activity.dao;

import by.epam.fitness.entity.exception.DAOException;

import java.util.List;

public interface IDAO<T> {

    List<T> getAll() throws DAOException;

    T get(int id) throws DAOException;

    void add(T t) throws DAOException;

    void delete(int id) throws DAOException;

    void update(int id, T t) throws DAOException;
}
