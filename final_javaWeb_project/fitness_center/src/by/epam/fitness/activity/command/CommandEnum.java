package by.epam.fitness.activity.command;

import by.epam.fitness.activity.command.admin.*;
import by.epam.fitness.activity.command.common.*;
import by.epam.fitness.activity.command.navigation.*;

public enum CommandEnum {
    LOGIN {
        {
            this.action = new LoginAction();
        }
    },
    REGISTER {
        {
            this.action = new RegistrationAction();
        }
    },
    LOGOUT {
        {
            this.action = new LogoutAction();
        }
    },
    LANGUAGE {
        {
            this.action = new ChangeLangAction();
        }
    },
    ADD_TRAINER {
        {
            this.action = new AddTrainerAction();
        }
    },
    ADD_SUBSCRIPTION {
        {
            this.action = new AddSubscriptionAction();
        }
    },
    ADD_TRAINING {
        {
            this.action = new AddTrainingAction();
        }
    },
    ADD_DIRECTION {
        {
            this.action = new AddDirectionAction();
        }
    },
    ADD_COURSE {
        {
            this.action = new AddCourseAction();
        }
    },
    ADD_GROUP {
        {
            this.action = new AddGroupAction();
        }
    },
    DELETE_TRAINER {
        {
            this.action = new DeleteTrainerAction();
        }
    },
    DELETE_TRAINING {
        {
            this.action = new DeleteTrainingAction();
        }
    },
    DELETE_DIRECTION {
        {
            this.action = new DeleteDirectionAction();
        }
    },
    DELETE_COURSE {
        {
            this.action = new DeleteCourseAction();
        }
    },
    DELETE_GROUP {
        {
            this.action = new DeleteGroupAction();
        }
    },
    DELETE_SUBSCRIPTION {
        {
            this.action = new DeleteSubscriptionAction();
        }
    },
    TO_MAIN {
        {
            this.action = new ToMainAction();
        }
    },
    TO_LOGIN {
        {
            this.action = new ToLoginAction();
        }
    },
    TO_REGISTRATION {
        {
            this.action = new ToRegistrationAction();
        }
    },
    TO_GROUPS {
        {
            this.action = new ToGroupsAction();
        }
    },
    TO_TRAINING {
        {
            this.action = new ToTrainingAction();
        }
    },
    TO_USERS {
        {
            this.action = new ToUsersAction();
        }
    },
    TO_USER {
        {
            this.action = new ToUserAction();
        }
    },
    TO_ERROR {
        {
            this.action = new ToErrorAction();
        }
    };

    ICommand action;

    public ICommand getAction() {
        return action;
    }
}
