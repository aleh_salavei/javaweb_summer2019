package by.epam.fitness.activity.command.admin;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOTrainer;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DeleteTrainerAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(DeleteTrainerAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User adminUser = (User) session.getAttribute("user");
        if (adminUser.getRole().getIdRole().equals("admin")) {
            DAOTrainer daoTrainer = new DAOTrainer();
            try {
                daoTrainer.delete(Integer.parseInt(request.getParameter("trainerID")));
            } catch (DAOException e) {
                logger.error(e);
            }
        }
        page = CommandEnum.TO_MAIN.getAction().execute(request);
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}