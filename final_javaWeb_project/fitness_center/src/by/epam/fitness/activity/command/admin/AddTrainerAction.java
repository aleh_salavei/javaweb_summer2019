package by.epam.fitness.activity.command.admin;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAODirection;
import by.epam.fitness.activity.dao.DAOTrainer;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Direction;
import by.epam.fitness.entity.table.Trainer;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AddTrainerAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(AddTrainerAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User adminUser = (User) session.getAttribute("user");
        if (adminUser.getRole().getIdRole().equals("admin")) {
            DAODirection daoDirection = new DAODirection();
            DAOTrainer daoTrainer = new DAOTrainer();
            Direction direction = null;
            try {
                direction = daoDirection.get(Integer.parseInt(request.getParameter("directionID_trainer")));
            } catch (DAOException e) {
                logger.error(e);
            }
            if (direction != null) {
                Trainer trainer = new Trainer(request.getParameter("firstName"),
                        request.getParameter("secondName"),
                        direction,
                        request.getParameter("trainerRank"));
                session.setAttribute("trainer", trainer);
                try {
                    daoTrainer.add(trainer);
                } catch (DAOException e) {
                    logger.error(e);
                }
            } else {
                request.setAttribute("addingFail", "Wrong direction ID!");
            }
            page = CommandEnum.TO_MAIN.getAction().execute(request);
        } else {
            page = "/page/error.jsp";
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}