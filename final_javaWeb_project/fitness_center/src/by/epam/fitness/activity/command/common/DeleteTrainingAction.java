package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOGroup;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOTraining;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Group;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.Training;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DeleteTrainingAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(DeleteTrainingAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        if (user.getRole().getIdRole().equals("user")) {
            DAOTraining daoTraining = new DAOTraining();
            DAOGroup daoGroup = new DAOGroup();
            DAOSubscription daoSubscription = new DAOSubscription();
            Group group = null;
            Subscription subscription = null;
            Training training = null;
            try {
                group = (Group) session.getAttribute("group");
                List<Subscription> subscriptions = daoSubscription.getAllByUser(user.getIdUser());
                for (Subscription sub : subscriptions) {
                    if (sub.getEndDate().compareTo(group.getDay()) >= 0 && sub.getTrainingQuantityLeft() > 0) {
                        subscription = sub;
                        break;
                    }
                }
                training = daoTraining.defineTraining(group.getIdGroup(), subscription.getIdSubscription());
                if (training != null) {
                    if (group.getLock().tryLock(1, TimeUnit.SECONDS)) {
                        group.setGroupFreeCapacity(group.getGroupFreeCapacity() + 1);
                        daoGroup.update(group.getIdGroup(), group);
                        subscription.setTrainingQuantityLeft(subscription.getTrainingQuantityLeft() + 1);
                        daoSubscription.update(subscription.getIdSubscription(), subscription);
                        daoTraining.delete(training.getIdTraining());
                    }
                    session.setAttribute("training", training);
                }
            } catch (DAOException | InterruptedException e) {
                logger.error(e);
            } finally {
                if (group.getLock().isHeldByCurrentThread())
                    group.getLock().unlock();
            }
            page = CommandEnum.TO_USER.getAction().execute(request);
        } else {
            page = "/page/main.jsp";
        }

        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}