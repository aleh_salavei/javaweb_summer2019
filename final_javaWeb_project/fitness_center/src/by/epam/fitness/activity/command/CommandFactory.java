package by.epam.fitness.activity.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class CommandFactory {

    private final static Logger logger = LogManager.getLogger(CommandFactory.class);

    public ICommand defineCommand(HttpServletRequest request) {
        ICommand current;
        String action = request.getParameter("command");
        try {
            logger.info(action);
            CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
            current = commandEnum.getAction();
            return current;
        } catch (IllegalArgumentException e) {
            logger.error(e);
            return CommandEnum.TO_ERROR.getAction();
        }
    }
}
