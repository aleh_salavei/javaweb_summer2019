package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToUsersAction implements ICommand {

    private static final Logger logger = LogManager.getLogger(ToUsersAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        if (user.getRole().getIdRole().equals("admin")) {
            page = "/page/users.jsp";
            List<User> users;
            DAOUser daoUser = new DAOUser();
            try {
                users = daoUser.getAll();
                session.setAttribute("users", users);
            } catch (DAOException e) {
                logger.error(e);
            }
            List<Subscription> subs;
            DAOSubscription daoSubscription = new DAOSubscription();
            try {
                subs = daoSubscription.getAll();
                session.setAttribute("subs", subs);
            } catch (DAOException e) {
                logger.error(e);
            }
        } else {
            page = "/page/main.jsp";
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}