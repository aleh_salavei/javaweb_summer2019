package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

public class ChangeLangAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter("lang");
        HttpSession session = request.getSession(true);
        String page = (String) session.getAttribute("page");
        switch (language) {
            case "en":
                session.setAttribute("locale", "en_US");
                session.setAttribute("bundle", ResourceBundle.getBundle("messages_en_US"));
                break;
            case "ru":
                session.setAttribute("locale", "ru_RU");
                session.setAttribute("bundle", ResourceBundle.getBundle("messages"));
                break;
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}