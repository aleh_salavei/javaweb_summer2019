package by.epam.fitness.activity.command.admin;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOCourse;
import by.epam.fitness.activity.dao.DAOGroup;
import by.epam.fitness.activity.dao.DAOTrainer;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Course;
import by.epam.fitness.entity.table.Group;
import by.epam.fitness.entity.table.Trainer;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Time;

public class AddGroupAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(AddGroupAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User adminUser = (User) session.getAttribute("user");
        if (adminUser.getRole().getIdRole().equals("admin")) {
            DAOTrainer daoTrainer = new DAOTrainer();
            DAOCourse daoCourse = new DAOCourse();
            DAOGroup daoGroup = new DAOGroup();
            Trainer trainer = null;
            Course course = null;
            try {
                trainer = daoTrainer.get(Integer.parseInt(request.getParameter("trainerID")));
                course = daoCourse.get(Integer.parseInt(request.getParameter("courseID")));
            } catch (DAOException e) {
                logger.error(e);
            }
            if (trainer != null && course != null) {
                System.out.println(request.getParameter("time"));
                Group group = new Group(trainer,
                        Date.valueOf(request.getParameter("day")),
                        Time.valueOf(request.getParameter("time") + ":00"),
                        Integer.parseInt(request.getParameter("groupCapacity")),
                        Integer.parseInt(request.getParameter("groupCapacity")),
                        course);
                session.setAttribute("group", group);
                try {
                    daoGroup.add(group);
                } catch (DAOException e) {
                    logger.error(e);
                }
            } else {
                request.setAttribute("groupAddFail", "Wrong trainer or course ID!");
            }
            page = CommandEnum.TO_GROUPS.getAction().execute(request);
        } else {
            page = CommandEnum.TO_MAIN.getAction().execute(request);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}