package by.epam.fitness.activity.command.admin;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAODirection;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Direction;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class AddDirectionAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(AddDirectionAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User adminUser = (User) session.getAttribute("user");
        if (adminUser.getRole().getIdRole().equals("admin")) {
            DAODirection daoDirection = new DAODirection();
            Direction direction = new Direction(request.getParameter("directionName"));
            session.setAttribute("direction", direction);
            try {
                daoDirection.add(direction);
            } catch (DAOException e) {
                logger.error(e);
            }
            page = CommandEnum.TO_MAIN.getAction().execute(request);
        } else {
            page = "/page/error.jsp";
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}