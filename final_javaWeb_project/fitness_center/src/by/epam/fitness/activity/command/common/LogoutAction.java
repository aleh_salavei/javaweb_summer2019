package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.ICommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/index.jsp";
        HttpSession session = request.getSession(true);
        session.invalidate();
        return page;
    }
}