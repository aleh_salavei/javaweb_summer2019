package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Role;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RegistrationAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(RegistrationAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User user = new User(request.getParameter("firstName"),
                request.getParameter("secondName"),
                request.getParameter("login"),
                request.getParameter("password"),
                request.getParameter("email"),
                Role.USER);
        DAOUser daoUser = new DAOUser();
        session.setAttribute("user", user);
        try {
            daoUser.add(user);
        } catch (DAOException e) {
            logger.error(e);
        }
        page = CommandEnum.TO_USER.getAction().execute(request);
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}