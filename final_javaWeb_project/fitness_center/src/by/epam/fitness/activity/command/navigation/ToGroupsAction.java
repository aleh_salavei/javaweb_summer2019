package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOGroup;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Group;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToGroupsAction implements ICommand {

    private static final Logger logger = LogManager.getLogger(ToGroupsAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/page/groups.jsp";
        HttpSession session = request.getSession(true);
        List<Group> groups;
        DAOGroup daoGroup = new DAOGroup();
        try {
            groups = daoGroup.getAll();
            session.setAttribute("groups", groups);
        } catch (DAOException e) {
            logger.error(e);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}