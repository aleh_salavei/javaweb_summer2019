package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOGroup;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOTraining;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Group;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.Training;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddTrainingAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(AddTrainingAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
        if (user.getRole().getIdRole().equals("user")) {
            DAOTraining daoTraining = new DAOTraining();
            DAOSubscription daoSubscription = new DAOSubscription();
            DAOGroup daoGroup = new DAOGroup();
            Group group = null;
            Subscription subscription = null;
            try {
                group = (Group) session.getAttribute("group");
                List<Subscription> subscriptions = daoSubscription.getAllByUser(user.getIdUser());
                for (Subscription sub : subscriptions) {
                    if (sub.getEndDate().compareTo(group.getDay()) >= 0 && sub.getTrainingQuantityLeft() > 0) {
                        subscription = sub;
                        break;
                    }
                }
            } catch (DAOException e) {
                logger.error(e);
            }
            if (group != null && subscription != null) {
                if (subscription.getTrainingQuantityLeft() > 0 && subscription.getEndDate().compareTo(new Date()) == 1 &&
                        subscription.getEndDate().compareTo(group.getDay()) == 1 && group.getDay().compareTo(new Date()) == 1) {
                    try {
                        if (group.getLock().tryLock(1, TimeUnit.SECONDS)) {
                            Training training = new Training(subscription, group);
                            session.setAttribute("training", training);
                            daoTraining.add(training);
                            group.setGroupFreeCapacity(group.getGroupFreeCapacity() - 1);
                            daoGroup.update(group.getIdGroup(), group);
                            subscription.setTrainingQuantityLeft(subscription.getTrainingQuantityLeft() - 1);
                            daoSubscription.update(subscription.getIdSubscription(), subscription);
                        }
                    } catch (InterruptedException | DAOException e) {
                        logger.error(e);
                    } finally {
                        if (group.getLock().isHeldByCurrentThread())
                            group.getLock().unlock();
                    }
                } else {
                    request.setAttribute("trainingAddFail", "Failed! Check your dates!");
                }
            } else {
                request.setAttribute("trainingAddFail", "Wrong group or subscription!");
            }
            page = CommandEnum.TO_USER.getAction().execute(request);
        } else {
            page = "/page/error.jsp";
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}