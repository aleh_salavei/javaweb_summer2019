package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOCourse;
import by.epam.fitness.activity.dao.DAOTrainer;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Course;
import by.epam.fitness.entity.table.Trainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToMainAction implements ICommand {

    private static final Logger logger = LogManager.getLogger(ToMainAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        page = "/page/main.jsp";
        List<Course> courses;
        DAOCourse daoCourse = new DAOCourse();
        try {
            courses = daoCourse.getAll();
            session.setAttribute("courses", courses);
        } catch (DAOException e) {
            logger.error(e);
        }
        List<Trainer> trainers;
        DAOTrainer daoTrainer = new DAOTrainer();
        try {
            trainers = daoTrainer.getAll();
            session.setAttribute("trainers", trainers);
        } catch (DAOException e) {
            logger.error(e);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}