package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ToLoginAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/page/login.jsp";
        HttpSession session = request.getSession(true);
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}