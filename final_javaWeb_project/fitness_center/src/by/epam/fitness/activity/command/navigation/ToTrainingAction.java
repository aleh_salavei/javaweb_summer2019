package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOGroup;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Group;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ToTrainingAction implements ICommand {

    private static final Logger logger = LogManager.getLogger(ToTrainingAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/page/training.jsp";
        HttpSession session = request.getSession(true);
        Group group;
        DAOGroup daoGroup = new DAOGroup();
        try {
            group = daoGroup.get(Integer.parseInt(request.getParameter("idGroup")));
            session.setAttribute("group", group);
        } catch (DAOException e) {
            logger.error(e);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}
