package by.epam.fitness.activity.command.admin;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class AddSubscriptionAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(AddSubscriptionAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        User adminUser = (User) session.getAttribute("user");
        if (adminUser.getRole().getIdRole().equals("admin")) {
            DAOUser daoUser = new DAOUser();
            DAOSubscription daoSubscription = new DAOSubscription();
            User user = null;
            try {
                user = daoUser.get(Integer.parseInt(request.getParameter("userID")));
            } catch (DAOException e) {
                logger.error(e);
            }
            if (user != null) {
                Subscription sub = new Subscription(user,
                        Date.valueOf(request.getParameter("startDate")),
                        Date.valueOf(request.getParameter("endDate")),
                        Integer.parseInt(request.getParameter("trainingQuantity")),
                        Integer.parseInt(request.getParameter("trainingQuantityLeft")));
                session.setAttribute("subscription", sub);
                try {
                    daoSubscription.add(sub);
                } catch (DAOException e) {
                    logger.error(e);
                }
            } else {
                request.setAttribute("addingFail", "Wrong user ID!");
            }
            page = CommandEnum.TO_USERS.getAction().execute(request);
        } else {
            page = CommandEnum.TO_MAIN.getAction().execute(request);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}