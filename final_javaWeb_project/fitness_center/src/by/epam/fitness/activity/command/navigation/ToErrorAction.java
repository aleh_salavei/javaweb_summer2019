package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ToErrorAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/page/error.jsp";
        HttpSession session = request.getSession(true);
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}