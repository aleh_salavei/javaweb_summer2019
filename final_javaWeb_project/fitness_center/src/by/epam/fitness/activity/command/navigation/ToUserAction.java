package by.epam.fitness.activity.command.navigation;

import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOSubscription;
import by.epam.fitness.activity.dao.DAOTraining;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.Subscription;
import by.epam.fitness.entity.table.Trainer;
import by.epam.fitness.entity.table.Training;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToUserAction implements ICommand {

    private static final Logger logger = LogManager.getLogger(ToUserAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        DAOSubscription daoSubscription = new DAOSubscription();
        DAOTraining daoTraining = new DAOTraining();
        User user = null;
        if (session.getAttribute("user") != null) {
            user = (User) session.getAttribute("user");
            try {
                List<Subscription> subscriptions = daoSubscription.getAllByUser(user.getIdUser());
                session.setAttribute("subscriptions", subscriptions);
                List<Training> trainings = daoTraining.getAllByUserID(user.getIdUser());
                session.setAttribute("trainings", trainings);
            } catch (DAOException e) {
                logger.error(e);
            }
            page = "/page/user.jsp";
        } else page = "/page/main.jsp";
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}