package by.epam.fitness.activity.command.common;

import by.epam.fitness.activity.command.CommandEnum;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.dao.DAOUser;
import by.epam.fitness.entity.exception.DAOException;
import by.epam.fitness.entity.table.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

public class LoginAction implements ICommand {

    private final static Logger logger = LogManager.getLogger(DeleteTrainingAction.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession(true);

        DAOUser daoUser = new DAOUser();
        User user;
        try {
            user = daoUser.defineUser(login, password);
            if (user != null) {
                session.setAttribute("user", user);
                page = "/index.jsp";
            } else {
                request.setAttribute("loginFail", ((ResourceBundle) session.getAttribute("bundle")).getString("loginFail"));
                page = "/page/login.jsp";
            }
        } catch (DAOException e) {
            page = "/page/error.jsp";
            logger.error(e);
        }
        session.setAttribute("page", page);
        session.setAttribute("lastCommand", request.getParameter("command"));
        return page;
    }
}