package by.epam.fitness.activity.listener;

import by.epam.fitness.entity.table.Role;
import by.epam.fitness.entity.table.User;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Locale;
import java.util.ResourceBundle;

@WebListener
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setAttribute("bundle", ResourceBundle.getBundle("messages"));
        se.getSession().setAttribute("locale", Locale.getDefault().toString());
        String page = "/index.jsp";
        se.getSession().setAttribute("page", page);
        User user = new User();
        user.setRole(Role.GUEST);
        se.getSession().setAttribute("user", user);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
    }
}
