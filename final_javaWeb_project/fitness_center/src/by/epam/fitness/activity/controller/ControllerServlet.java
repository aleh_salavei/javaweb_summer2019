package by.epam.fitness.activity.controller;

import by.epam.fitness.activity.command.CommandFactory;
import by.epam.fitness.activity.command.ICommand;
import by.epam.fitness.activity.connection.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller")
public class ControllerServlet extends HttpServlet {

    private final static Logger logger = LogManager.getLogger(ControllerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().closeConnections();
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String page;
        CommandFactory client = new CommandFactory();
        ICommand action = client.defineCommand(req);
        page = action.execute(req);
        if (page != null) {
            RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher(page);
            dispatcher.forward(req, resp);
        } else {
            page = "/page/error.jsp";
            resp.sendRedirect(req.getContextPath() + page);
            logger.error("result page is null");
        }
    }

}