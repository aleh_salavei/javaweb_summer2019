package by.epam.fitness.entity.table;

import java.util.Objects;

public class Direction {

    private int idDirection;
    private String name;

    public Direction() {
    }

    public Direction(String name) {
        this.name = name;
    }

    public int getIdDirection() {
        return idDirection;
    }

    public void setIdDirection(int idDirection) {
        this.idDirection = idDirection;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direction direction = (Direction) o;
        return idDirection == direction.idDirection &&
                Objects.equals(name, direction.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDirection, name);
    }

    @Override
    public String toString() {
        return "Direction{" +
                "idDirection=" + idDirection +
                ", name='" + name + '\'' +
                '}';
    }
}
