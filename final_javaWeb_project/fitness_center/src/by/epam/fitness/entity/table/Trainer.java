package by.epam.fitness.entity.table;

import java.util.Objects;

public class Trainer {

    private int idTrainer;
    private String firstName;
    private String secondName;
    private Direction direction;
    private String rank;

    public Trainer() {
    }

    public Trainer(String firstName, String secondName, Direction direction, String rank) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.direction = direction;
        this.rank = rank;
    }

    public int getIdTrainer() {
        return idTrainer;
    }

    public void setIdTrainer(int idTrainer) {
        this.idTrainer = idTrainer;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainer trainer = (Trainer) o;
        return idTrainer == trainer.idTrainer &&
                Objects.equals(firstName, trainer.firstName) &&
                Objects.equals(secondName, trainer.secondName) &&
                Objects.equals(direction, trainer.direction) &&
                Objects.equals(rank, trainer.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTrainer, firstName, secondName, direction, rank);
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "idTrainer=" + idTrainer +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", direction=" + direction +
                ", rank='" + rank + '\'' +
                '}';
    }
}
