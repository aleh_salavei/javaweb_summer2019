package by.epam.fitness.entity.table;

import java.sql.Date;
import java.sql.Time;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Group {

    private int idGroup;
    private Trainer trainer;
    private Date day;
    private Time time;
    private int groupCapacity;
    private int groupFreeCapacity;
    private Course course;
    private ReentrantLock lock = new ReentrantLock();

    public Group() {
    }

    public Group(Trainer trainer, Date day, Time time, int groupCapacity, int groupFreeCapacity, Course course) {
        this.trainer = trainer;
        this.day = day;
        this.time = time;
        this.groupCapacity = groupCapacity;
        this.groupFreeCapacity = groupFreeCapacity;
        this.course = course;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getGroupCapacity() {
        return groupCapacity;
    }

    public void setGroupCapacity(int groupCapacity) {
        this.groupCapacity = groupCapacity;
    }

    public int getGroupFreeCapacity() {
        return groupFreeCapacity;
    }

    public void setGroupFreeCapacity(int groupFreeCapacity) {
        this.groupFreeCapacity = groupFreeCapacity;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public ReentrantLock getLock() {
        return lock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return idGroup == group.idGroup &&
                groupCapacity == group.groupCapacity &&
                groupFreeCapacity == group.groupFreeCapacity &&
                Objects.equals(trainer, group.trainer) &&
                Objects.equals(day, group.day) &&
                Objects.equals(time, group.time) &&
                Objects.equals(course, group.course);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGroup, trainer, day, time, groupCapacity, groupFreeCapacity, course);
    }

    @Override
    public String toString() {
        return "Group{" +
                "idGroup=" + idGroup +
                ", trainer=" + trainer +
                ", day=" + day +
                ", time=" + time +
                ", groupCapacity=" + groupCapacity +
                ", groupFreeCapacity=" + groupFreeCapacity +
                ", course=" + course +
                '}';
    }
}
