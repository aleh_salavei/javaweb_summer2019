package by.epam.fitness.entity.table;

public enum Role {

    ADMIN("admin"),
    USER("user"),
    GUEST("guest");

    String idRole;

    Role(String idRole) {
        this.idRole = idRole;
    }

    public String getIdRole() {
        return idRole;
    }


    @Override
    public String toString() {
        return "Role{" +
                "idRole=" + idRole +
                "} " + super.toString();
    }
}
