package by.epam.fitness.entity.table;

import java.sql.Date;
import java.util.Objects;

public class Subscription {

    private int idSubscription;
    private User user;
    private Date startDate;
    private Date endDate;
    private int trainingQuantity;
    private int trainingQuantityLeft;

    public Subscription() {
    }

    public Subscription(User user, Date startDate, Date endDate, int trainingQuantity, int trainingQuantityLeft) {
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
        this.trainingQuantity = trainingQuantity;
        this.trainingQuantityLeft = trainingQuantityLeft;
    }

    public int getIdSubscription() {
        return idSubscription;
    }

    public void setIdSubscription(int idSubscription) {
        this.idSubscription = idSubscription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getTrainingQuantity() {
        return trainingQuantity;
    }

    public void setTrainingQuantity(int trainingQuantity) {
        this.trainingQuantity = trainingQuantity;
    }

    public int getTrainingQuantityLeft() {
        return trainingQuantityLeft;
    }

    public void setTrainingQuantityLeft(int trainingQuantityLeft) {
        this.trainingQuantityLeft = trainingQuantityLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return idSubscription == that.idSubscription &&
                trainingQuantity == that.trainingQuantity &&
                trainingQuantityLeft == that.trainingQuantityLeft &&
                Objects.equals(user, that.user) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSubscription, user, startDate, endDate, trainingQuantity, trainingQuantityLeft);
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "idSubscription=" + idSubscription +
                ", user=" + user +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", trainingQuantity=" + trainingQuantity +
                ", trainingQuantityLeft=" + trainingQuantityLeft +
                '}';
    }
}
