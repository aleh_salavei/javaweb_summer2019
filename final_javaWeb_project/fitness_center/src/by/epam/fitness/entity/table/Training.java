package by.epam.fitness.entity.table;

import java.util.Objects;

public class Training {

    private int idTraining;
    private Subscription subscription;
    private Group group;

    public Training() {
    }

    public Training(Subscription subscription, Group group) {
        this.subscription = subscription;
        this.group = group;
    }

    public int getIdTraining() {
        return idTraining;
    }

    public void setIdTraining(int idTraining) {
        this.idTraining = idTraining;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Training training = (Training) o;
        return idTraining == training.idTraining &&
                Objects.equals(subscription, training.subscription) &&
                Objects.equals(group, training.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTraining, subscription, group);
    }

    @Override
    public String toString() {
        return "Training{" +
                "idTraining=" + idTraining +
                ", subscription=" + subscription +
                ", group=" + group +
                '}';
    }
}
