package by.epam.fitness.entity.table;

import java.util.Objects;

public class Course {

    private int idCourse;
    private Direction direction;
    private String name;
    private float rating;
    private String rank;

    public Course() {
    }

    public Course(Direction direction, String name, float rating, String rank) {
        this.direction = direction;
        this.name = name;
        this.rating = rating;
        this.rank = rank;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return idCourse == course.idCourse &&
                Float.compare(course.rating, rating) == 0 &&
                Objects.equals(direction, course.direction) &&
                Objects.equals(name, course.name) &&
                Objects.equals(rank, course.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCourse, direction, name, rating, rank);
    }

    @Override
    public String toString() {
        return "Course{" +
                "idCourse=" + idCourse +
                ", direction=" + direction +
                ", name='" + name + '\'' +
                ", rating=" + rating +
                ", rank='" + rank + '\'' +
                '}';
    }
}
