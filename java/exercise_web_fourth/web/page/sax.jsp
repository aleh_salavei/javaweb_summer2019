<%@ page import="by.epam.summer.exercise.fourth.entity.Medicine" %>
<%@ page import="by.epam.summer.exercise.fourth.activity.parser.XMLParser" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Oleg
  Date: 24.06.2019
  Time: 15:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SAX</title>
</head>
<body>
<table border="1">
    <caption>Таблица медикаментов (SAX)</caption>
    <tr>
        <th>Название</th>
        <th>Производитель</th>
        <th>Штрих-код</th>
        <th>Группа</th>
        <th>Форма</th>
        <th>Сертификат</th>
        <th>Дозировка</th>
        <th>Упаковка</th>
        <th>Аналоги</th>
    </tr>
    <%
        ArrayList<Medicine> medicines = XMLParser.parseSAX(config.getServletContext().getRealPath("/"));
        for (Medicine medicine: medicines) {
    %>
    <tr>
        <td><%=medicine.getName()%></td>
        <td><%=medicine.getPharmName()%></td>
        <td><%=medicine.getID()%></td>
        <td><%=medicine.getGroup()%></td>
        <td><%=medicine.getVersions()%></td>
        <td><%=medicine.getCertificate()%></td>
        <td><%=medicine.getDosage()%></td>
        <td><%=medicine.getPackage()%></td>
        <td><%=medicine.getAnalogs()%></td>
    </tr>
    <%
        }
    %>
</table>
<form action="../index.jsp">
    <input type="submit" value="Back to the roots">
</form>
</body>
</html>
