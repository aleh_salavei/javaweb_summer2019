package by.epam.summer.exercise.fourth.activity.handler;

import by.epam.summer.exercise.fourth.activity.creator.MedicineCreator;
import by.epam.summer.exercise.fourth.entity.Medicine;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;

public class SAXHandler extends DefaultHandler {

    private ArrayList<Medicine> medicines = new ArrayList<>();
    private Medicine medicine;
    private String lastElementName;

    @Override
    public void startDocument() {
    }

    @Override
    public void endDocument() {

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        lastElementName = qName;
        if (qName.equals("medicine"))
            medicine = MedicineCreator.createMedicine(attributes.getValue("name"),
                    attributes.getValue("group"));
        if (qName.equals("versions"))
            medicine = MedicineCreator.createVersion(medicine, attributes.getValue("version"));
        if (qName.equals("pharm"))
            medicine = MedicineCreator.createPharm(medicine, attributes.getValue("name"),
                    attributes.getValue("id"));
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals("pharm"))
            medicines.add(medicine);
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String information = (new String(ch, start, length)).replace("\n", "").trim();
        if (!information.isEmpty()) {
            if (lastElementName.equals("analogs"))
                MedicineCreator.addAnalogs(medicine, information);
            if (lastElementName.equals("number"))
                MedicineCreator.addNumber(medicine, information);
            if (lastElementName.equals("startDate"))
                MedicineCreator.addStartDate(medicine, information);
            if (lastElementName.equals("endDate"))
                MedicineCreator.addEndDate(medicine, information);
            if (lastElementName.equals("organization"))
                MedicineCreator.addOrganization(medicine, information);
            if (lastElementName.equals("type"))
                MedicineCreator.addPackType(medicine, information);
            if (lastElementName.equals("quantity"))
                MedicineCreator.addQuantity(medicine, information);
            if (lastElementName.equals("price"))
                MedicineCreator.addPrice(medicine, information);
            if (lastElementName.equals("dose"))
                MedicineCreator.addDosage(medicine, information);
            if (lastElementName.equals("timesPerDay"))
                MedicineCreator.addDosagePerDay(medicine, information);
            if (lastElementName.equals("days"))
                MedicineCreator.addDays(medicine, information);
        }
    }

    public ArrayList<Medicine> getMedicines() {
        return medicines;
    }
}
