package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.activity.handler.SAXHandler;
import by.epam.summer.exercise.fourth.entity.Medicine;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class SAXParser implements IXMLParserBuilder {

    @Override
    public ArrayList<Medicine> parseXML(String path) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser = factory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(new File(path), handler);
        return handler.getMedicines();
    }
}
