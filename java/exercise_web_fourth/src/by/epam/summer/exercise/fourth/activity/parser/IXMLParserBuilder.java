package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.entity.Medicine;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;

public interface IXMLParserBuilder {

    ArrayList<Medicine> parseXML(String path) throws ParserConfigurationException, SAXException,
            IOException, XMLStreamException;
}
