package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.activity.creator.MedicineCreator;
import by.epam.summer.exercise.fourth.entity.Medicine;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;

import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

class STAXParser implements IXMLParserBuilder {

    public ArrayList<Medicine> parseXML(String path) throws XMLStreamException, IOException {
        ArrayList<Medicine> medicines = new ArrayList<>();
        Medicine medicine;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(path));
        while (xmlEventReader.hasNext()) {
            XMLEvent event = xmlEventReader.nextEvent();
            if (event.isStartElement()) {
                String eventName = event.asStartElement().getName().getLocalPart();
                if (eventName.equals("medicine")) {
                    Iterator<Attribute> attributeIterator = event.asStartElement().getAttributes();
                    medicine = MedicineCreator.createMedicine(attributeIterator.next().getValue(),
                            attributeIterator.hasNext() ? attributeIterator.next().getValue() : "other");

                    while (xmlEventReader.hasNext()) {
                        event = xmlEventReader.nextEvent();
                        if (event.isCharacters()) {
                            String content = event.asCharacters().getData().replace("\n", "").trim();
                            if (!content.isEmpty()) {
                                if (eventName.equals("analogs"))
                                    MedicineCreator.addAnalogs(medicine, content);
                                if (eventName.equals("number"))
                                    MedicineCreator.addNumber(medicine, content);
                                if (eventName.equals("startDate"))
                                    MedicineCreator.addStartDate(medicine, content);
                                if (eventName.equals("endDate"))
                                    MedicineCreator.addEndDate(medicine, content);
                                if (eventName.equals("organization"))
                                    MedicineCreator.addOrganization(medicine, content);
                                if (eventName.equals("type"))
                                    MedicineCreator.addPackType(medicine, content);
                                if (eventName.equals("quantity"))
                                    MedicineCreator.addQuantity(medicine, content);
                                if (eventName.equals("price"))
                                    MedicineCreator.addPrice(medicine, content);
                                if (eventName.equals("dose"))
                                    MedicineCreator.addDosage(medicine, content);
                                if (eventName.equals("timesPerDay"))
                                    MedicineCreator.addDosagePerDay(medicine, content);
                                if (eventName.equals("days"))
                                    MedicineCreator.addDays(medicine, content);
                            }
                        }
                        if (event.isStartElement()) {
                            eventName = event.asStartElement().getName().getLocalPart();
                            if (eventName.equals("versions")) {
                                attributeIterator = event.asStartElement().getAttributes();
                                medicine = MedicineCreator.createVersion(medicine, attributeIterator.next().getValue());
                            }
                            if (eventName.equals("pharm")) {
                                attributeIterator = event.asStartElement().getAttributes();
                                medicine = MedicineCreator.createPharm(medicine, attributeIterator.next().getValue(),
                                        attributeIterator.next().getValue());
                            }
                        }
                        if (event.isEndElement()) {
                            eventName = event.asEndElement().getName().getLocalPart();
                            if (eventName.equals("pharm"))
                                medicines.add(medicine);
                            if (eventName.equals("medicine"))
                                break;
                        }
                    }
                }
            }
        }
        return medicines;
    }
}