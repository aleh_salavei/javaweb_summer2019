package by.epam.summer.exercise.fourth.entity;

public enum MedicinePack {
    BOX("box"),
    BOTTLE("bottle"),
    JAR("jar"),
    BLISTER("blister"),
    MEMBRANE("membrane"),
    OTHER("other");

    private String value;
    MedicinePack(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MedicinePack getPack(String value){
        for (MedicinePack pack: MedicinePack.values()) {
            if (pack.getValue().equals(value))
                return pack;
        }
        return OTHER;
    }
}
