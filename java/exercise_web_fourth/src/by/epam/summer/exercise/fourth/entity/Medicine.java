package by.epam.summer.exercise.fourth.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

public class Medicine implements Cloneable{
    private String name;
    private MedicineGroup group;
    private ArrayList<String> analogs;
    private MedicineForm versions;
    private MedicineID medicineID;

    public Medicine(String name, MedicineGroup group) {
        this.name = name;
        this.group = group;
        analogs = new ArrayList<>();
        medicineID = new MedicineID();
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getAnalogs() {
        return analogs;
    }

    public MedicineForm getVersions() {
        return versions;
    }

    public MedicineGroup getGroup() {
        return group;
    }

    public String getID() {
        return medicineID.medicineID;
    }

    public MedicineID.MedicineCertificate getCertificate() {
        return medicineID.certificate;
    }

    public MedicineID.MedicineDosage getDosage() {
        return medicineID.medicineDosage;
    }

    public MedicineID.MedicinePackage getPackage() {
        return medicineID.medicinePackage;
    }

    public void setVersions(MedicineForm versions) {
        this.versions = versions;
    }

    public String getPharmName() {
        return medicineID.pharmName;
    }

    public void setPharmName(String pharmName) {
        this.medicineID.pharmName = pharmName;
    }
    public void setMedicineID(String medicineID) {
        this.medicineID.medicineID = medicineID;
    }

    public void setCertificateNumber(String value){
        medicineID.certificate.number = value;
    }
    public void setCertificateStart(LocalDate value){
        medicineID.certificate.startDate = value;
    }

    public void setCertificateEnd(LocalDate value){
        medicineID.certificate.endDate = value;
    }

    public void setCertificateOrganization(String value){
        medicineID.certificate.organizationName = value;
    }

    public void setMedicinePackageType(MedicinePack packageType) {
        this.medicineID.medicinePackage.packType = packageType;
    }

    public void setMedicinePackageQuantity(int packageQuantity) {
        this.medicineID.medicinePackage.quantityInPack = packageQuantity;
    }

    public void setMedicinePackagePrice(double packageType) {
        this.medicineID.medicinePackage.price = packageType;
    }

    public void setMedicineDosagePerOnce(double medicineDosagePerOnce) {
        this.medicineID.medicineDosage.dosagePerOnce = medicineDosagePerOnce;
    }

    public void setMedicineDosagePerDay(int medicineDosagePerDay) {
        this.medicineID.medicineDosage.timesPerDay = medicineDosagePerDay;
    }

    public void setMedicineDosageDays(int medicineDosageDays) {
        this.medicineID.medicineDosage.quantityOfDays = medicineDosageDays;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Medicine cloned = (Medicine)super.clone();
        cloned.medicineID = (MedicineID)cloned.medicineID.clone();
        return cloned;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "name: '" + name + '\'' +
                ", group: " + group +
                ", analogs: " + analogs +
                ", versions: " + versions +
                ", pharmName: '" + medicineID.pharmName + '\'' +
                ", medicineID: '" + medicineID.medicineID + '\'' +
                "; certificate: " + medicineID.certificate +
                "; medicinePackage: " + medicineID.medicinePackage +
                "; medicineDosage: " + medicineID.medicineDosage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Medicine)) return false;
        Medicine medicine = (Medicine) o;
        return Objects.equals(getName(), medicine.getName()) &&
                getGroup() == medicine.getGroup() &&
                Objects.equals(getAnalogs(), medicine.getAnalogs()) &&
                getVersions() == medicine.getVersions() &&
                Objects.equals(medicineID, medicine.medicineID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getGroup(), getAnalogs(), getVersions(), medicineID);
    }

    private class MedicineID implements Cloneable{
        private String pharmName;
        private String medicineID;
        private MedicineCertificate certificate;
        private MedicinePackage medicinePackage;
        private MedicineDosage medicineDosage;

        MedicineID(){
            certificate = new MedicineCertificate();
            medicinePackage = new MedicinePackage();
            medicineDosage = new MedicineDosage();
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            MedicineID cloned = (MedicineID)super.clone();
            cloned.certificate = (MedicineCertificate)cloned.certificate.clone();
            cloned.medicinePackage = (MedicinePackage)cloned.medicinePackage.clone();
            cloned.medicineDosage = (MedicineDosage) cloned.medicineDosage.clone();
            return cloned;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MedicineID)) return false;
            MedicineID that = (MedicineID) o;
            return Objects.equals(pharmName, that.pharmName) &&
                    Objects.equals(medicineID, that.medicineID) &&
                    Objects.equals(certificate, that.certificate) &&
                    Objects.equals(medicinePackage, that.medicinePackage) &&
                    Objects.equals(medicineDosage, that.medicineDosage);
        }

        @Override
        public int hashCode() {
            return Objects.hash(pharmName, medicineID, certificate, medicinePackage, medicineDosage);
        }

        private class MedicineCertificate implements Cloneable{
            private String number;
            private LocalDate startDate;
            private LocalDate endDate;
            private String organizationName;

            @Override
            public String toString() {
                return "number: '" + number + '\'' +
                        ", startDate: " + startDate +
                        ", endDate: " + endDate +
                        ", organizationName: '" + organizationName + '\'';
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof MedicineCertificate)) return false;
                MedicineCertificate that = (MedicineCertificate) o;
                return Objects.equals(number, that.number) &&
                        Objects.equals(startDate, that.startDate) &&
                        Objects.equals(endDate, that.endDate) &&
                        Objects.equals(organizationName, that.organizationName);
            }

            @Override
            public int hashCode() {
                return Objects.hash(number, startDate, endDate, organizationName);
            }
        }

        private class MedicinePackage implements Cloneable{
            private MedicinePack packType;
            private int quantityInPack;
            private double price;

            @Override
            public String toString() {
                return "packType: " + packType +
                        ", quantityInPack: " + quantityInPack +
                        ", price: " + price;
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof MedicinePackage)) return false;
                MedicinePackage that = (MedicinePackage) o;
                return quantityInPack == that.quantityInPack &&
                        Double.compare(that.price, price) == 0 &&
                        packType == that.packType;
            }

            @Override
            public int hashCode() {
                return Objects.hash(packType, quantityInPack, price);
            }
        }

        private class MedicineDosage implements Cloneable{
            private double dosagePerOnce;
            private int timesPerDay;
            private int quantityOfDays;

            @Override
            public String toString() {
                return  "dosagePerOnce: " + dosagePerOnce +
                        ", timesPerDay: " + timesPerDay +
                        ", quantityOfDays: " + quantityOfDays;
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof MedicineDosage)) return false;
                MedicineDosage that = (MedicineDosage) o;
                return Double.compare(that.dosagePerOnce, dosagePerOnce) == 0 &&
                        timesPerDay == that.timesPerDay &&
                        quantityOfDays == that.quantityOfDays;
            }

            @Override
            public int hashCode() {
                return Objects.hash(dosagePerOnce, timesPerDay, quantityOfDays);
            }
        }
    }
}