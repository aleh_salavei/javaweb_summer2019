package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.activity.creator.MedicineCreator;
import by.epam.summer.exercise.fourth.entity.Medicine;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class DOMParser implements IXMLParserBuilder {

    public ArrayList<Medicine> parseXML(String path) throws IOException, SAXException, ParserConfigurationException {
        ArrayList<Medicine> medicines = new ArrayList<>();
        Medicine medicine;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(path));

        NodeList medicineList = document.getElementsByTagName("medicine");
        for (int i = 0; i < medicineList.getLength(); i++) {
            Element medicineElement = (Element) medicineList.item(i);
            NamedNodeMap attributes = medicineElement.getAttributes();
            medicine = MedicineCreator.createMedicine(attributes.getNamedItem("name").getNodeValue(),
                    (attributes.getLength() > 1) ? attributes.getNamedItem("group").getNodeValue() : "other");
            NodeList analogElements = medicineElement.getElementsByTagName("analogs");
            for (int j = 0; j < analogElements.getLength(); j++) {
                MedicineCreator.addAnalogs(medicine, analogElements.item(j).getTextContent());
            }

            NodeList versionElements = medicineElement.getElementsByTagName("versions");
            for (int j = 0; j < versionElements.getLength(); j++) {
                Element versionElement = (Element) versionElements.item(j);
                medicine = MedicineCreator.createVersion(medicine, versionElement.getAttributes()
                        .getNamedItem("version").getNodeValue());

                NodeList pharmElements = versionElement.getElementsByTagName("pharm");
                for (int k = 0; k < pharmElements.getLength(); k++) {
                    Element pharmElement = (Element) pharmElements.item(k);
                    medicine = MedicineCreator.createPharm(medicine, pharmElement.getAttributes().getNamedItem("name")
                            .getNodeValue(), pharmElement.getAttributes().getNamedItem("id").getNodeValue());

                    NodeList certificateElements = pharmElement.getElementsByTagName("certificate");
                    Element certificateElement = (Element) certificateElements.item(0);
                    MedicineCreator.addNumber(medicine, certificateElement.getElementsByTagName("number")
                            .item(0).getTextContent());
                    MedicineCreator.addStartDate(medicine, certificateElement.getElementsByTagName("startDate")
                            .item(0).getTextContent());
                    MedicineCreator.addEndDate(medicine, certificateElement.getElementsByTagName("endDate")
                            .item(0).getTextContent());
                    MedicineCreator.addOrganization(medicine, certificateElement.getElementsByTagName("organization")
                            .item(0).getTextContent());

                    NodeList packageElements = pharmElement.getElementsByTagName("package");
                    Element packageElement = (Element) packageElements.item(0);
                    MedicineCreator.addPackType(medicine, packageElement.getElementsByTagName("type")
                            .item(0).getTextContent());
                    MedicineCreator.addQuantity(medicine, packageElement.getElementsByTagName("quantity")
                            .item(0).getTextContent());
                    MedicineCreator.addPrice(medicine, packageElement.getElementsByTagName("price")
                            .item(0).getTextContent());

                    NodeList dosageElements = pharmElement.getElementsByTagName("dosage");
                    Element dosageElement = (Element) dosageElements.item(0);
                    MedicineCreator.addDosage(medicine, dosageElement.getElementsByTagName("dose")
                            .item(0).getTextContent());
                    MedicineCreator.addDosagePerDay(medicine, dosageElement.getElementsByTagName("timesPerDay")
                            .item(0).getTextContent());
                    MedicineCreator.addDays(medicine, dosageElement.getElementsByTagName("days")
                            .item(0).getTextContent());

                    medicines.add(medicine);
                }
            }
        }
        return medicines;
    }
}