package by.epam.summer.exercise.fourth.entity;

public enum MedicineForm {
    TABLETS("tablets"),
    SYRUP("syrup"),
    SUSPENSION("suspension"),
    SPRAY("spray"),
    OTHER("other");

    private String value;
    MedicineForm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MedicineForm getForm(String value){
        for (MedicineForm form: MedicineForm.values()) {
            if (form.getValue().equals(value))
                return form;
        }
        return OTHER;
    }
}