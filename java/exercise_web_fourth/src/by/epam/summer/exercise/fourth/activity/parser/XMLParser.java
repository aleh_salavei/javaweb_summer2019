package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.activity.validator.XMLValidator;
import by.epam.summer.exercise.fourth.entity.Medicine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;


public class XMLParser {

    private static final Logger LOGGER = LogManager.getLogger(XMLParser.class);

    public static ArrayList<Medicine> parseSAX(String path) {
        LOGGER.info("SAX Parsing selected");
        ArrayList<Medicine> resultList = new ArrayList<>();
        try {
            if (XMLValidator.valid(path))
                resultList = new SAXParser().parseXML(path + "/resource/pharmacy.xml");
        } catch (IOException e) {
            LOGGER.error("SAX parsing failed with IOException: " + e);
        } catch (SAXException e) {
            LOGGER.error("SAX parsing failed with SAXException: " + e);
        } catch (ParserConfigurationException e) {
            LOGGER.error("SAX parsing failed with ParserConfigurationException: " + e);
        }

        return resultList;
    }

    public static ArrayList<Medicine> parseSTAX(String path) {
        LOGGER.info("STAX Parsing selected");
        ArrayList<Medicine> resultList = new ArrayList<>();
        try {
            if (XMLValidator.valid(path))
                resultList = new STAXParser().parseXML(path + "/resource/pharmacy.xml");
        } catch (IOException e) {
            LOGGER.error("StAX parsing failed with IOException: " + e);
        } catch (XMLStreamException e) {
            LOGGER.error("StAX parsing failed with XMLStreamException: " + e);
        }
        return resultList;
    }

    public static ArrayList<Medicine> parseDOM(String path) {
        LOGGER.info("DOM Parsing selected");
        ArrayList<Medicine> resultList = new ArrayList<>();
        try {
            if (XMLValidator.valid(path))
                resultList = new DOMParser().parseXML(path + "/resource/pharmacy.xml");
        } catch (IOException e) {
            LOGGER.error("DOM parsing failed with IOException: " + e);
        } catch (SAXException e) {
            LOGGER.error("DOM parsing failed with SAXException: " + e);
        } catch (ParserConfigurationException e) {
            LOGGER.error("DOM parsing failed with ParserConfigurationException: " + e);
        }
        return resultList;
    }

}
