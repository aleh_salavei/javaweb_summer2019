package by.epam.summer.exercise.fourth.activity.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLValidator {

    private static final Logger LOGGER = LogManager.getLogger(XMLValidator.class);

    public static boolean valid(String path) {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        String fileName = path + "resource\\pharmacy.xml";
        String schemaName = path + "resource\\pharmacy.xsd";
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(schemaName);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);
            LOGGER.info(fileName + " is valid.");
            return true;
        } catch (SAXException e) {
            LOGGER.error("validation " + fileName + " is not valid because " + e.getMessage());
            return false;
        } catch (IOException e) {
            LOGGER.error(fileName + " is not valid because " + e.getMessage());
            return false;
        }
    }

}
