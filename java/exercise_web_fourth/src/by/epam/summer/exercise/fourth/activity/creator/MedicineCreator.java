package by.epam.summer.exercise.fourth.activity.creator;

import by.epam.summer.exercise.fourth.entity.Medicine;
import by.epam.summer.exercise.fourth.entity.MedicineForm;
import by.epam.summer.exercise.fourth.entity.MedicineGroup;
import by.epam.summer.exercise.fourth.entity.MedicinePack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MedicineCreator {

    private static final Logger LOGGER = LogManager.getLogger(MedicineCreator.class);

    public static Medicine createMedicine(String name, String group){
        LOGGER.debug("new " + name + " medicine created");
        return new Medicine(name, MedicineGroup.getGroup(group));
    }

    public static Medicine createVersion(Medicine medicine, String version){
        if(medicine.getVersions() == null){
            medicine.setVersions(MedicineForm.getForm(version));
            LOGGER.debug("version of " + medicine.getName() + " medicine added");
        } else{
            try {
                medicine = (Medicine)medicine.clone();
            } catch (CloneNotSupportedException e) {
                LOGGER.error("version creation failed with " + e);
            }
            medicine.setVersions(MedicineForm.getForm(version));
            LOGGER.debug("new version of " + medicine.getName() + " medicine created");
        }
        return medicine;
    }

    public static Medicine createPharm(Medicine medicine, String name, String id){
        if (medicine.getPharmName() == null) {
            medicine.setPharmName(name);
            LOGGER.debug("pharm of " + medicine.getName() + " medicine added");
            medicine.setMedicineID(id);
            LOGGER.debug("id of " + medicine.getName() + " medicine added");
        } else {
            try {
                medicine = (Medicine)medicine.clone();
            } catch (CloneNotSupportedException e) {
                LOGGER.error("model creation failed with " + e);
            }
            medicine.setPharmName(name);
            medicine.setMedicineID(id);
            LOGGER.debug("new model of " + medicine.getName() + " medicine added");
        }
        return medicine;
    }

    public static void addAnalogs(Medicine medicine, String analogName){
        LOGGER.debug("analog of " + medicine.getName() + " medicine added");
        medicine.getAnalogs().add(analogName);
    }

    public static void addNumber(Medicine medicine, String number){
        LOGGER.debug("certificate number of " + medicine.getName() + " medicine added");
        medicine.setCertificateNumber(number);
    }

    public static void addStartDate(Medicine medicine, String date){
        LOGGER.debug("certificate start date of " + medicine.getName() + " medicine added");
        medicine.setCertificateStart(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    public static void addEndDate(Medicine medicine, String date){
        LOGGER.debug("certificate end date of " + medicine.getName() + " medicine added");
        medicine.setCertificateEnd(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    public static void addOrganization(Medicine medicine, String organizationName){
        LOGGER.debug("certificate organization of " + medicine.getName() + " medicine added");
        medicine.setCertificateOrganization(organizationName);
    }

    public static void addPackType(Medicine medicine, String packType){
        LOGGER.debug("package of " + medicine.getName() + " medicine added");
        medicine.setMedicinePackageType(MedicinePack.getPack(packType));
    }

    public static void addQuantity(Medicine medicine, String quantity){
        LOGGER.debug("quantity of " + medicine.getName() + " medicine added");
        medicine.setMedicinePackageQuantity(Integer.parseInt(quantity));
    }

    public static void addPrice(Medicine medicine, String price){
        LOGGER.debug("price of " + medicine.getName() + " medicine added");
        medicine.setMedicinePackagePrice(Double.parseDouble(price));
    }

    public static void addDosage(Medicine medicine, String dosage){
        LOGGER.debug("dosage of " + medicine.getName() + " medicine added");
        medicine.setMedicineDosagePerOnce(Double.parseDouble(dosage));
    }

    public static void addDosagePerDay(Medicine medicine, String dosagePerDay){
        LOGGER.debug("times per day of " + medicine.getName() + " medicine added");
        medicine.setMedicineDosagePerDay(Integer.parseInt(dosagePerDay));
    }

    public static void addDays(Medicine medicine, String days){
        LOGGER.debug("times per circle of " + medicine.getName() + " medicine added");
        medicine.setMedicineDosageDays(Integer.parseInt(days));
    }
}