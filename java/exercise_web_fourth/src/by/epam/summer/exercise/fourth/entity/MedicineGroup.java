package by.epam.summer.exercise.fourth.entity;

public enum MedicineGroup {
    PAINKILLER("painkiller"),
    VITAMIN("vitamin"),
    ANTIBIOTICS("antibiotics"),
    HISTAMINE("histamine"),
    ANTIPHLOGISTIC("antiphlogistic"),
    OTHER("other");

    private String value;
    MedicineGroup(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MedicineGroup getGroup(String value){
        for (MedicineGroup group: MedicineGroup.values()) {
            if (group.getValue().equals(value))
                return group;
        }
        return OTHER;
    }
}
