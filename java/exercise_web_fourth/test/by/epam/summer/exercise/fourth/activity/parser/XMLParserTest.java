package by.epam.summer.exercise.fourth.activity.parser;

import by.epam.summer.exercise.fourth.entity.Medicine;
import by.epam.summer.exercise.fourth.entity.MedicineForm;
import by.epam.summer.exercise.fourth.entity.MedicineGroup;
import by.epam.summer.exercise.fourth.entity.MedicinePack;
import org.testng.annotations.*;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class XMLParserTest {
    private ArrayList<Medicine> medicines;
    private Medicine medicine;
    private String path = System.getProperty("user.dir") + "\\test\\by\\epam\\summer\\exercise\\fourth\\" +
            "activity\\parser\\";

    @BeforeClass
    public void setUp() {
        medicines = new ArrayList<>();
        medicine = new Medicine("1", MedicineGroup.VITAMIN);
        medicine.getAnalogs().add("analog 1");
        medicine.getAnalogs().add("analog 2");
        medicine.setVersions(MedicineForm.SPRAY);
        medicine.setPharmName("pharm 1");
        medicine.setMedicineID("i12345");
        medicine.setCertificateNumber("AA12A123");
        medicine.setCertificateStart(LocalDate.parse("2019-01-01"));
        medicine.setCertificateEnd(LocalDate.parse("2019-10-01"));
        medicine.setCertificateOrganization("organization 1");
        medicine.setMedicinePackageType(MedicinePack.JAR);
        medicine.setMedicinePackageQuantity(10);
        medicine.setMedicinePackagePrice(10.25);
        medicine.setMedicineDosagePerOnce(0.5);
        medicine.setMedicineDosagePerDay(3);
        medicine.setMedicineDosageDays(2);
        medicines.add(medicine);
        medicine = new Medicine("1", MedicineGroup.VITAMIN);
        medicine.getAnalogs().add("analog 1");
        medicine.getAnalogs().add("analog 2");
        medicine.setVersions(MedicineForm.SYRUP);
        medicine.setPharmName("pharm 1");
        medicine.setMedicineID("i12346");
        medicine.setCertificateNumber("AA12A124");
        medicine.setCertificateStart(LocalDate.parse("2019-01-02"));
        medicine.setCertificateEnd(LocalDate.parse("2019-10-02"));
        medicine.setCertificateOrganization("organization 2");
        medicine.setMedicinePackageType(MedicinePack.BOTTLE);
        medicine.setMedicinePackageQuantity(11);
        medicine.setMedicinePackagePrice(10.36);
        medicine.setMedicineDosagePerOnce(1);
        medicine.setMedicineDosagePerDay(4);
        medicine.setMedicineDosageDays(5);
        medicines.add(medicine);
        medicine = new Medicine("1", MedicineGroup.VITAMIN);
        medicine.getAnalogs().add("analog 1");
        medicine.getAnalogs().add("analog 2");
        medicine.setVersions(MedicineForm.SYRUP);
        medicine.setPharmName("pharm 2");
        medicine.setMedicineID("i12347");
        medicine.setCertificateNumber("AA12A125");
        medicine.setCertificateStart(LocalDate.parse("2019-01-03"));
        medicine.setCertificateEnd(LocalDate.parse("2019-10-03"));
        medicine.setCertificateOrganization("organization 3");
        medicine.setMedicinePackageType(MedicinePack.BOTTLE);
        medicine.setMedicinePackageQuantity(12);
        medicine.setMedicinePackagePrice(10.37);
        medicine.setMedicineDosagePerOnce(12);
        medicine.setMedicineDosagePerDay(40);
        medicine.setMedicineDosageDays(50);
        medicines.add(medicine);
    }

    @AfterClass
    public void tearDown() {
        medicines = null;
        medicine = null;
        path = null;
    }

    @Test
    public void testParseSAX() {
        assertEquals(XMLParser.parseSAX(path), medicines);
    }

    @Test
    public void testParseSTAX() {
        assertEquals(XMLParser.parseSTAX(path), medicines);
    }

    @Test
    public void testParseDOM() {
        assertEquals(XMLParser.parseDOM(path), medicines);
    }
}
