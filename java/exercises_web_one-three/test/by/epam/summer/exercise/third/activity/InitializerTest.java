package by.epam.summer.exercise.third.activity;

import by.epam.summer.exercise.third.entity.matrix.MatrixElement;
import by.epam.summer.exercise.third.entity.exception.OutOfRangeException;
import by.epam.summer.exercise.third.entity.thread.MatrixHandlerThread;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class InitializerTest {

    @Test
    public void testInitMatrix() throws Exception {
        MatrixElement[][] matrix = Initializer.initMatrix("x = 8, y = 4, z = 7");
        MatrixElement[] diagonal = new MatrixElement[matrix.length];
        int[] diagonalInt = new int[diagonal.length];
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j) {
                    diagonal[i] = matrix[i][j];
                    diagonalInt[i] = diagonal[i].getValue();
                }
            }
        }
        assertEquals(diagonalInt, new int[]{0,0,0,0,0,0,0,0});
    }

    @Test(expectedExceptions = OutOfRangeException.class)
    public void testInitMatrixLess() throws Exception {
        Initializer.initMatrix("x = 1, y = 4, z = 7");
    }

    @Test(expectedExceptions = OutOfRangeException.class)
    public void testInitMatrixMore() throws Exception {
        Initializer.initMatrix("x = 15, y = 4, z = 7");
    }

    @Test
    public void testInitThreads() throws Exception {
        ArrayList<Thread> threadList = new ArrayList<>();
        threadList.add(new MatrixHandlerThread(1));
        threadList.add(new MatrixHandlerThread(2));
        threadList.add(new MatrixHandlerThread(3));
        threadList.add(new MatrixHandlerThread(4));
        assertEquals(Initializer.initThreads("x = 1, y = 4, z = 7"), threadList);
    }

    @Test(expectedExceptions = OutOfRangeException.class)
    public void testInitThreadsLess() throws Exception {
        Initializer.initThreads("x = 1, y = 3, z = 7");
    }

    @Test(expectedExceptions = OutOfRangeException.class)
    public void testInitThreadsMore() throws Exception {
        Initializer.initThreads("x = 1, y = 8, z = 7");
    }
}