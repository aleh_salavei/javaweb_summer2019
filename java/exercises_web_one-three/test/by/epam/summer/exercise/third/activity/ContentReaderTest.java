package by.epam.summer.exercise.third.activity;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.testng.Assert.*;

public class ContentReaderTest {
    private String str = "One = 1, two = 2One = 1, two = 2";
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\third\\resource\\";

    @BeforeMethod
    public void setUp() throws Exception {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(resourceFolder + "test_content.txt")));
        String str = "One = 1, two = 2";
        writer.write(str);
        writer.newLine();
        str = "One = 1, two = 2";
        writer.write(str);
        writer.close();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        Path path = Paths.get(resourceFolder + "test_content.txt");
        Files.deleteIfExists(path);
        str = null;
        resourceFolder = null;
    }

    @Test
    public void testReadFile() {
        assertEquals(ContentReader.readFile( "test_content.txt"), str);
    }
}