package by.epam.summer.exercise.second.activity;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.testng.Assert.*;

public class TextReaderTest {
    private String str = "\t\tOne.\t\tTwo. Three. Four.\t\tFive. Six.\t\tSeven.";
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\second\\resource\\";

    @BeforeMethod
    public void setUp() throws Exception {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(resourceFolder + "test_content.txt")));
        String str = "\t" + "One.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Two. Three. Four.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Five. Six.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Seven.";
        writer.write(str);
        writer.close();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        Path path = Paths.get(resourceFolder + "test_content.txt");
        Files.deleteIfExists(path);
        str = null;
        resourceFolder = null;
    }

    @Test
    public void testContentReader() {
        assertEquals(TextReader.contentReader("test_content.txt"), str);
    }
}