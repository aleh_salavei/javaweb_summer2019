package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Sentence;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class SentenceParserTest {

    @Test
    public void testParse() {
        SentenceParser sParser = new SentenceParser();
        ArrayList<Text> sArray = new ArrayList<>();
        sArray.add(new Sentence("On! e."));
        sArray.add(new Sentence("T. wo!"));
        sArray.add(new Sentence("Thr? ee."));
        assertEquals(sParser.parse("\tOn! e. T. wo!\tThr? ee.", TextType.SENTENCE), sArray);
    }
}