package by.epam.summer.exercise.second.activity;

import by.epam.summer.exercise.second.entity.Paragraph;
import by.epam.summer.exercise.second.entity.Sentence;
import by.epam.summer.exercise.second.entity.Text;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class ParagraphComparatorTest {
    private Text bigParagraph = new Paragraph("\tOne.\tTwo.");
    private Text smallParagraph = new Paragraph("\tThree.");
    private Text equalsSmallParagraph = new Paragraph("\tThree.");

    @BeforeClass
    public void setUp(){
        this.bigParagraph.getSeparatedText().add(new Sentence("One."));
        this.bigParagraph.getSeparatedText().add(new Sentence("Two."));
        this.smallParagraph.getSeparatedText().add(new Sentence("Three."));
        this.equalsSmallParagraph.getSeparatedText().add(new Sentence("Three."));
    }

    @AfterClass
    public void tearDown() {
        bigParagraph = null;
        smallParagraph = null;
    }

    @Test
    public void testCompareMore() {
        assertEquals(new ParagraphComparator().compare(bigParagraph, smallParagraph), 1);
    }

    @Test
    public void testCompareLess() {
        assertEquals(new ParagraphComparator().compare(smallParagraph, bigParagraph), -1);
    }

    @Test
    public void testCompareEqual() {
        assertEquals(new ParagraphComparator().compare(equalsSmallParagraph, smallParagraph), 0);
    }
}