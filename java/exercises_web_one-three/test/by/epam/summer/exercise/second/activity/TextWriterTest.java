package by.epam.summer.exercise.second.activity;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.testng.Assert.*;

public class TextWriterTest {
    private String str = "\t\tOne.\t\tTwo. Three. Four.\t\tFive. Six.\t\tSeven.";
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\second\\resource\\";

    @AfterMethod
    public void tearDown() throws Exception {
        Path path = Paths.get(resourceFolder + "test_content.txt");
        Files.deleteIfExists(path);
        str = null;
        resourceFolder = null;
    }

    @Test
    public void testContentWriter() {
        TextWriter.contentWriter("test_content.txt", new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
        assertEquals(TextReader.contentReader("test_content.txt"), str);
    }
}