package by.epam.summer.exercise.second.activity;

import by.epam.summer.exercise.second.entity.*;
import org.testng.annotations.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class TextHandlerTest {
    private String str = "\t\tOne.\t\tTwo. Three. Four.\t\tFive. Six.\t\tSeven.";
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\second\\resource\\";

    @BeforeClass
    public void setUp() throws Exception {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(resourceFolder + "test_content.txt")));
        BufferedWriter shortWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(resourceFolder + "test_short_content.txt")));
        String str = "\t" + "One.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Two. Three. Four.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Five. Six.";
        writer.write(str);
        writer.newLine();
        str = "\t" + "Seven.";
        writer.write(str);
        writer.close();
        str = "\t" + "Tw, th. Fo.";
        shortWriter.write(str);
        shortWriter.close();
    }

    @AfterClass
    public void tearDown() throws Exception {
        Path path = Paths.get(resourceFolder + "test_content.txt");
        Files.deleteIfExists(path);
        path = Paths.get(resourceFolder + "test_short_content.txt");
        Files.deleteIfExists(path);
        str = null;
        resourceFolder = null;
    }

    @Test
    public void testReadText() {
        assertEquals(TextHandler.readText("test_content.txt"),
                new GlobalText(str));
    }

    @Test
    public void testParseTextToParagraphs(){
        GlobalText gt = TextHandler.readText("test_content.txt");
        ArrayList<Text> testArray = new ArrayList<>();
        testArray.add(new Paragraph("One."));
        testArray.add(new Paragraph("Two. Three. Four."));
        testArray.add(new Paragraph("Five. Six."));
        testArray.add(new Paragraph("Seven."));
        assertEquals(TextHandler.parseText(gt, TextType.PARAGRAPH), testArray);
    }

    @Test
    public void testParseTextToSentences(){
        GlobalText gt = TextHandler.readText("test_content.txt");
        ArrayList<Text> pArray = new ArrayList<>();
        ArrayList<Text> sArray = new ArrayList<>();
        sArray.add(new Sentence("One."));
        pArray.add(new Paragraph(sArray));
        sArray.clear();
        sArray.add(new Sentence("Two."));
        sArray.add(new Sentence("Three."));
        sArray.add(new Sentence("Four."));
        pArray.add(new Paragraph(sArray));
        sArray.clear();
        sArray.add(new Sentence("Five."));
        sArray.add(new Sentence("Six."));
        pArray.add(new Paragraph(sArray));
        sArray.clear();
        sArray.add(new Sentence("Seven."));
        pArray.add(new Paragraph(sArray));
        assertEquals(TextHandler.parseText(gt, TextType.SENTENCE), pArray);
    }

    @Test
    public void testParseTextToLexemes(){
        GlobalText gt = TextHandler.readText("test_short_content.txt");
        ArrayList<Text> pArray = new ArrayList<>();
        ArrayList<Text> sArray = new ArrayList<>();
        ArrayList<Text> lArray = new ArrayList<>();
        lArray.add(new Lexeme("Tw,"));
        lArray.add(new Lexeme("th."));
        sArray.add(new Sentence(lArray));
        lArray.clear();
        lArray.add(new Lexeme("Fo."));
        sArray.add(new Sentence(lArray));
        pArray.add(new Paragraph(sArray));
        assertEquals(TextHandler.parseText(gt, TextType.LEXEME), pArray);
    }

    @Test
    public void testParseTextToWords(){
        GlobalText gt = TextHandler.readText("test_short_content.txt");
        ArrayList<Text> pArray = new ArrayList<>();
        ArrayList<Text> sArray = new ArrayList<>();
        ArrayList<Text> lArray = new ArrayList<>();
        ArrayList<Text> wArray = new ArrayList<>();
        ArrayList<Text> symArray = new ArrayList<>();
        wArray.add(new Word("Tw"));
        symArray.add(new Symbol(','));
        wArray.add(new Lexeme(symArray));
        lArray.add(new Lexeme(wArray));
        wArray.clear();
        symArray.clear();
        wArray.add(new Word("th"));
        symArray.add(new Symbol('.'));
        wArray.add(new Lexeme(symArray));
        lArray.add(new Lexeme(wArray));
        sArray.add(new Sentence(lArray));
        wArray.clear();
        lArray.clear();
        symArray.clear();
        wArray.add(new Word("Fo"));
        symArray.add(new Symbol('.'));
        wArray.add(new Lexeme(symArray));
        lArray.add(new Lexeme(wArray));
        sArray.add(new Sentence(lArray));
        pArray.add(new Paragraph(sArray));
        assertEquals(TextHandler.parseText(gt, TextType.WORD), pArray);
    }

    @Test
    public void testParseTextToSymbols(){
        GlobalText gt = TextHandler.readText("test_short_content.txt");
        ArrayList<Text> pArray = new ArrayList<>();
        ArrayList<Text> sArray = new ArrayList<>();
        ArrayList<Text> lArray = new ArrayList<>();
        ArrayList<Text> symArray = new ArrayList<>();
        symArray.add(new Symbol('T'));
        symArray.add(new Symbol('w'));
        symArray.add(new Symbol(','));
        lArray.add(new Lexeme(symArray));
        symArray.clear();
        symArray.add(new Symbol('t'));
        symArray.add(new Symbol('h'));
        symArray.add(new Symbol('.'));
        lArray.add(new Lexeme(symArray));
        sArray.add(new Sentence(lArray));
        symArray.clear();
        lArray.clear();
        symArray.add(new Symbol('F'));
        symArray.add(new Symbol('o'));
        symArray.add(new Symbol('.'));
        lArray.add(new Lexeme(symArray));
        sArray.add(new Sentence(lArray));
        pArray.add(new Paragraph(sArray));
        assertEquals(TextHandler.parseText(gt, TextType.SYMBOL), pArray);
    }

    @Test
    public void testSortParagraphs() throws Exception {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.WORD);
        GlobalText newGT = TextHandler.sortParagraphs(gt);
        assertEquals(newGT.toString(), "\tOne.\tSeven.\tFive. Six.\tTwo. Three. Four.");
    }

    @Test(expectedExceptions = UnsortableException.class)
    public void testSortUnsortableParagraphs() throws Exception {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.PARAGRAPH);
        TextHandler.sortParagraphs(gt);
    }

    @Test
    public void testGenerateTextFromParagraph() {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.PARAGRAPH);
        assertEquals(TextHandler.generateText(gt.toString()), new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
    }

    @Test
    public void testGenerateTextFromSentence() {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.SENTENCE);
        assertEquals(TextHandler.generateText(gt.toString()), new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
    }

    @Test
    public void testGenerateTextFromLexeme() {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.LEXEME);
        assertEquals(TextHandler.generateText(gt.toString()), new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
    }

    @Test
    public void testGenerateTextFromWord() {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.WORD);
        assertEquals(TextHandler.generateText(gt.toString()), new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
    }

    @Test
    public void testGenerateTextFromSymbol() {
        GlobalText gt = TextHandler.readText("test_content.txt");
        TextHandler.parseText(gt, TextType.SYMBOL);
        assertEquals(TextHandler.generateText(gt.toString()), new String[]{"One.", "Two. Three. Four.", "Five. Six.", "Seven."});
    }
}