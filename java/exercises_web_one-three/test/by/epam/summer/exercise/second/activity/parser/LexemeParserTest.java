package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Lexeme;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class LexemeParserTest {

    @Test
    public void testParse() {
        LexemeParser lParser = new LexemeParser();
        ArrayList<Text> lArray = new ArrayList<>();
        lArray.add(new Lexeme("One."));
        lArray.add(new Lexeme("Two."));
        assertEquals(lParser.parse("\tOne. Two.", TextType.LEXEME), lArray);
    }
}