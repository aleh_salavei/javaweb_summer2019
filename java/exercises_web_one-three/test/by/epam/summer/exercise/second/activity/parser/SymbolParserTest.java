package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.*;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class SymbolParserTest {

    @Test
    public void testParse() {
        SymbolParser symParser = new SymbolParser();
        ArrayList<Text> symArray = new ArrayList<>();
        symArray.add(new Symbol('"'));
        symArray.add(new Symbol('O'));
        symArray.add(new Symbol('n'));
        symArray.add(new Symbol('e'));
        symArray.add(new Symbol('.'));
        assertEquals(symParser.parse("\"One.", TextType.WORD), symArray);
    }
}