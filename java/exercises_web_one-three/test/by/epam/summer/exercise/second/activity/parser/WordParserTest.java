package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.*;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class WordParserTest {

    @Test
    public void testParse(){
        WordParser wParser = new WordParser();
        ArrayList<Text> wArray = new ArrayList<>();
        ArrayList<Text> symArray = new ArrayList<>();
        wArray.add(new Word("One"));
        symArray.add(new Symbol('.'));
        wArray.add(new Lexeme(symArray));
        assertEquals(wParser.parse("One.", TextType.WORD), wArray);
    }
}