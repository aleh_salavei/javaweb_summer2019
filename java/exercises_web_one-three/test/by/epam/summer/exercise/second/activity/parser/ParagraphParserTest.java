package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Paragraph;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class ParagraphParserTest {

    @Test
    public void testParse() {
        ParagraphParser pParser = new ParagraphParser();
        ArrayList<Text> pArray = new ArrayList<>();
        pArray.add(new Paragraph("One. Two."));
        pArray.add(new Paragraph("Three."));
        assertEquals(pParser.parse("\tOne. Two.\tThree.", TextType.PARAGRAPH), pArray);
    }
}