package by.epam.summer.exercise.first.activity;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.testng.Assert.assertEquals;

public class TxtFileReaderTest {
    private String[][] lines = {{"BALL", "BIG", "orange ball", "2.5"}};
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\first\\resource\\";

    @BeforeMethod
    public void setUp() throws Exception {
        String line = "BALL, BIG, orange ball, 2.5";
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(resourceFolder + "test_content.txt")));
        writer.write(line);
        writer.close();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        Path path = Paths.get(resourceFolder + "test_content.txt");
        Files.deleteIfExists(path);
        lines = null;
        resourceFolder = null;
    }

    @Test
    public void testGetContent() {
        assertEquals(TxtFileReader.getContent("test_content.txt"), lines);
    }

    @Test
    public void testGetNullContent() {
        assertEquals(TxtFileReader.getContent("test_content1.txt"), new String[0][0]);
    }
}