package by.epam.summer.exercise.first.activity;

import by.epam.summer.exercise.first.entity.exception.NotEnoughMoneyException;
import by.epam.summer.exercise.first.entity.room.PlayRoom;
import by.epam.summer.exercise.first.entity.toy.*;
import org.testng.annotations.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class RoomMaintenanceTest {

    private RoomMaintenance rm = new RoomMaintenance();
    private String path = "test_content.txt";
    private PlayRoom testRoom = new PlayRoom(20, "testRoom");
    private String resourceFolder = System.getProperty("user.dir") +
            "\\src\\by\\epam\\summer\\exercise\\first\\resource\\";

    @BeforeClass
    public void setUp() throws Exception {
        String line = "BALL, BIG, orange ball, 2.5, resin";
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resourceFolder + path)));
        writer.write(line + "\n");
        line = "CAR, MIDDLE, mercedes, 7, true";
        writer.write(line + "\n");
        line = "CAR, 25, 10, 7, false";
        writer.write(line + "\n");
        line = "CAR, BIG, volvo, 5, false";
        writer.write(line);
        writer.close();
    }

    @AfterClass
    public void tearDown() throws Exception {
        Files.deleteIfExists(Paths.get(resourceFolder + path));
        rm = null;
        testRoom = null;
        this.path = null;
        this.resourceFolder = null;
    }

    @Test
    public void testFillCatalog() {
        rm.fillCatalog(path);
        ArrayList<AbstractToy> testList = new ArrayList<>();
        testList.add(new BallToy(ToySize.BIG, "orange ball", 2.5, "resin"));
        testList.add(new CarToy(ToySize.MIDDLE, "mercedes", 7, true));
        testList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        assertEquals(rm.getToyCatalog(), testList);
    }

    @Test
    public void testCreateRoom() {
        assertEquals(rm.createRoom(20, "testRoom"), testRoom);
    }

    @Test(dependsOnMethods = {"testCreateRoom", "testFillCatalog"})
    public void testAddToy() throws Exception {
        rm.addToy(testRoom, "volvo");
        rm.addToy(testRoom, "mercedes");
        rm.addToy(testRoom, "orange ball");
        rm.addToy(testRoom, "mercede");
        assertTrue(testRoom.getToyList().contains(new CarToy(ToySize.BIG, "volvo", 5, false)));
    }

    @Test(dependsOnMethods = {"testAddToy"})
    public void testAddWrongToy() {
        assertFalse(testRoom.getToyList().contains(new CarToy(ToySize.BIG, "Wrong Toy", 5, true)));
    }

    @Test(dependsOnMethods = "testFillCatalog", expectedExceptions = NotEnoughMoneyException.class)
    public void testAddToyException() throws Exception {
        PlayRoom cheapTestRoomRoom = rm.createRoom(1, "cheap test room");
        rm.addToy(cheapTestRoomRoom, "volvo");
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testDeleteToy() throws Exception {
        rm.deleteToy(testRoom, "volvo");
        assertFalse(testRoom.getToyList().contains(new CarToy(ToySize.BIG, "volvo", 5, false)));
        rm.addToy(testRoom, "volvo");
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testDeleteWrongToy() {
        rm.deleteToy(testRoom, "Wrong Toy");
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testGetToyFromRoom() {
        assertEquals(rm.getToyFromRoom(testRoom, "volvo").orElse(null),
                new CarToy(ToySize.BIG, "volvo", 5, false));
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testGetWrongToyFromRoom() {
        assertEquals(rm.getToyFromRoom(testRoom, "Wrong Toy").orElse(null),null);
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testGetToyFromRoom1() {
        ArrayList<AbstractToy> toyList = new ArrayList<>();
        toyList.add(new CarToy(ToySize.MIDDLE, "mercedes", 7, true));
        toyList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        assertEquals(rm.getToyFromRoom(testRoom, ToyType.CAR), toyList);
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testGetToyFromRoom2() {
        ArrayList<AbstractToy> toyList = new ArrayList<>();
        toyList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        assertEquals(rm.getToyFromRoom(testRoom, ToyType.CAR, ToySize.BIG), toyList);
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testGetToyFromRoom3() {
        ArrayList<AbstractToy> toyList = new ArrayList<>();
        toyList.add(new BallToy(ToySize.BIG, "orange ball", 2.5, "resin"));
        toyList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        assertEquals(rm.getToyFromRoom(testRoom, 6), toyList);
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testSortToysByPrice() {
        ArrayList<AbstractToy> toyList = new ArrayList<>();
        toyList.add(new BallToy(ToySize.BIG, "orange ball", 2.5, "resin"));
        toyList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        toyList.add(new CarToy(ToySize.MIDDLE, "mercedes", 7, true));
        rm.sortToysByPrice(testRoom);
        assertEquals(testRoom.getToyList(), toyList);
    }

    @Test(dependsOnMethods = "testAddToy")
    public void testSortToysByTypeAndSize() {
        ArrayList<AbstractToy> toyList = new ArrayList<>();
        toyList.add(new BallToy(ToySize.BIG, "orange ball", 2.5, "resin"));
        toyList.add(new CarToy(ToySize.MIDDLE, "mercedes", 7, true));
        toyList.add(new CarToy(ToySize.BIG, "volvo", 5, false));
        rm.sortToysByTypeAndSize(testRoom);
        assertEquals(testRoom.getToyList(), toyList);
    }
}