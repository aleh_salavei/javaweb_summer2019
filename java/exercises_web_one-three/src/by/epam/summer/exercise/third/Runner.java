package by.epam.summer.exercise.third;


import by.epam.summer.exercise.third.activity.ContentReader;
import by.epam.summer.exercise.third.activity.Initializer;
import by.epam.summer.exercise.third.entity.matrix.SingletonMatrix;
import by.epam.summer.exercise.third.entity.exception.OutOfRangeException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class Runner {

    private final static Logger logger = Logger.getLogger(Runner.class);

    public static void main(String[] args) {

        logger.info("program starts");
        ArrayList<Thread> threadList = new ArrayList<>(6);

        try {
            threadList = Initializer.initThreads(ContentReader.readFile("third_content.txt"));
        } catch (OutOfRangeException e) {
            logger.error(e + " the number of threads must be between 4 and 6");
        }

        logger.info(SingletonMatrix.getInstance());

        for (Thread t: threadList) {
            t.start();
        }

        for (Thread t: threadList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        logger.info(SingletonMatrix.getInstance());
        logger.info("program ends");

    }
}
