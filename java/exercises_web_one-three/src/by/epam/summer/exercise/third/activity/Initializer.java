package by.epam.summer.exercise.third.activity;

import by.epam.summer.exercise.third.entity.matrix.MatrixElement;
import by.epam.summer.exercise.third.entity.thread.MatrixHandlerThread;
import by.epam.summer.exercise.third.entity.exception.OutOfRangeException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Initializer {

    private final static Logger logger = Logger.getLogger(Initializer.class);

    public static MatrixElement[][] initMatrix(String initData) throws OutOfRangeException{
        logger.info("matrix initialization starts");
        ArrayList<Integer> dataList = getInitValues(initData);
        MatrixElement[][] matrix;
        if (dataList.get(0) >= 8 && dataList.get(0) <= 12) {
            matrix = new MatrixElement[dataList.get(0)][dataList.get(0)];
        } else{
            throw new OutOfRangeException("wrong matrix size");
        }
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[i].length; j++){
                if (i == j){
                    matrix[i][j] = new MatrixElement(0);
                } else{
                    matrix[i][j] = new MatrixElement((int) (Math.random()*100));
                }
            }
        }
        logger.info("matrix initialization successfully ends");
        return matrix;
    }

    public static ArrayList<Thread> initThreads(String initData) throws OutOfRangeException{
        logger.info("threads initialization starts");
        ArrayList<Integer> dataList = getInitValues(initData);
        ArrayList<Thread> threadList= new ArrayList<>();
        if (dataList.get(1) >= 4 && dataList.get(1) <= 6) {
            for(int i = 0; i < dataList.get(1); i++)
                threadList.add(new MatrixHandlerThread(i+1));
        } else{
            throw new OutOfRangeException("wrong number of threads");
        }
        logger.info("threads initialization successfully ends");
        return threadList;
    }

    private static ArrayList<Integer> getInitValues(String initData){
        logger.info("parsing data");
        String REGEX = "\\d+";
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(initData);
        ArrayList<Integer> dataList = new ArrayList<>(2);
        while (matcher.find()) {
            dataList.add(Integer.parseInt(matcher.group()));
        }
        return dataList;
    }

}
