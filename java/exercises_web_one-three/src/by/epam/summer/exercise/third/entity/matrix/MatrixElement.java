package by.epam.summer.exercise.third.entity.matrix;

import java.util.concurrent.locks.ReentrantLock;

public class MatrixElement {

    private ReentrantLock lock;
    private int value;

    public MatrixElement(int value){
        this.lock = new ReentrantLock();
        this.value = value;
    }

    public ReentrantLock getLock() {
        return lock;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
