package by.epam.summer.exercise.third.entity.matrix;

import by.epam.summer.exercise.third.activity.ContentReader;
import by.epam.summer.exercise.third.activity.Initializer;
import by.epam.summer.exercise.third.entity.exception.OutOfRangeException;
import org.apache.log4j.Logger;

public final class SingletonMatrix {

    private final static Logger logger = Logger.getLogger(SingletonMatrix.class);
    private MatrixElement[][] matrix;

    private SingletonMatrix() throws OutOfRangeException{
        try {
            this.matrix = Initializer.initMatrix(ContentReader.readFile("third_content.txt"));
        } catch (OutOfRangeException e) {
            this.matrix = Initializer.initMatrix("12");
        }
    }

    public static SingletonMatrix getInstance() {
        return InnerSingletonMatrix.instance;
    }

    public MatrixElement[][] getMatrix() {
        return matrix;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++){
                if (i == j)
                 result = result + "\t" + matrix[i][j];
            }
        }
        return result.trim();
    }

    private static class InnerSingletonMatrix{
        private static SingletonMatrix instance;
        static {
            try {
                instance = new SingletonMatrix();
            } catch (OutOfRangeException e) {
                logger.error(e + " the size of matrix has to be between 8 and 12");
                logger.info("matrix with default size was created");
            }
        }
    }
}