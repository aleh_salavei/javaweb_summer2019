package by.epam.summer.exercise.third.activity;


import org.apache.log4j.Logger;

import java.io.*;


public class ContentReader {

    private static final Logger logger = Logger.getLogger(ContentReader.class);

    public static String readFile(String path){
        logger.info("file reading starts");
        String info = "";
        String resourceFolder = System.getProperty("user.dir") + "\\src\\by\\epam\\summer\\exercise\\third\\resource\\";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader
                (new FileInputStream(resourceFolder + path)))){
            String str;
            while ((str = bufferedReader.readLine()) != null){
                info = info.concat(str);
            }
        } catch (FileNotFoundException e) {
            logger.error("there's no such file " + e);
        } catch (IOException e){
            logger.error("file damaged " + e);
        }
        logger.info("file read successful");
        return info;
    }
}
