package by.epam.summer.exercise.third.entity.exception;

public class OutOfRangeException extends Exception {

    public OutOfRangeException(String message) {
        super(message);
    }
}
