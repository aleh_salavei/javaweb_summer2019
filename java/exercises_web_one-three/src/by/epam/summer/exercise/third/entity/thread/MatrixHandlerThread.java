package by.epam.summer.exercise.third.entity.thread;

import by.epam.summer.exercise.third.entity.matrix.MatrixElement;
import by.epam.summer.exercise.third.entity.matrix.SingletonMatrix;
import org.apache.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MatrixHandlerThread extends Thread {
    private final static Logger logger = Logger.getLogger(MatrixHandlerThread.class);
    private int threadNumber;

    public MatrixHandlerThread(int number){
        this.threadNumber = number;
    }

    @Override
    public void run() {
        SingletonMatrix singletonMatrix = SingletonMatrix.getInstance();
        MatrixElement[][] matrix = singletonMatrix.getMatrix();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++){
                if (i == j){
                    try {
                        logger.debug(Thread.currentThread().getName() + " trying to in (" + i + ", " + j + ")");
                        if (matrix[i][j].getLock().tryLock(5, TimeUnit.MILLISECONDS)) {
                            logger.debug(Thread.currentThread().getName() + " is " +
                                    Thread.currentThread().getState() + " in (" + i + ", " + j + ") where value is "
                                    + matrix[i][j]);
                            if (matrix[i][j].getValue() == 0) {
                                matrix[i][j].setValue(threadNumber);
                                logger.debug(Thread.currentThread().getName() + " in (" + i + ", " + j + ") sets "
                                        + matrix[i][j]);
                                join(20);
                            }
                        }
                    }catch(InterruptedException e){
                        logger.error(e);
                    } finally {
                        logger.debug(Thread.currentThread().getName() + " quit from (" + i + ", " + j + ")");
                        if (matrix[i][j].getLock().isHeldByCurrentThread())
                            matrix[i][j].getLock().unlock();
                    }
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatrixHandlerThread)) return false;
        MatrixHandlerThread that = (MatrixHandlerThread) o;
        return threadNumber == that.threadNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(threadNumber);
    }
}
