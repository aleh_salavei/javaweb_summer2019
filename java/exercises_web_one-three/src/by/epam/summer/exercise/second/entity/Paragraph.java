package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Paragraph implements Text{
    private ArrayList<Text> separatedText;
    private String content;

    public Paragraph(String text){
        this.content = text;
        this.separatedText = new ArrayList<>();
    }

    public Paragraph(ArrayList<Text> text) {
        separatedText = new ArrayList<>();
        separatedText.addAll(text);
        this.content = this.toString();
    }

    public ArrayList<Text> getSeparatedText() {
        return separatedText;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String toString(){
        Iterator<Text> iterator = separatedText.iterator();
        String result = "";
        while (iterator.hasNext())
            result = result.concat(" ").concat(iterator.next().getContent().trim());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Paragraph)) return false;
        Paragraph paragraph = (Paragraph) o;
        return Objects.equals(getSeparatedText(), paragraph.getSeparatedText()) &&
                Objects.equals(getContent(), paragraph.getContent());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getSeparatedText(), getContent());
    }
}
