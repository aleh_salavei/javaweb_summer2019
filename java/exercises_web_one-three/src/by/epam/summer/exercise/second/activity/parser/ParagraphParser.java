package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Paragraph;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class ParagraphParser implements Parser {

    private static int pParserCounter = 0;
    private final String REGEX = "\\t+";
    private static final Logger log = Logger.getLogger(ParagraphParser.class);

    public ParagraphParser() {
        pParserCounter++;
    }

    public static int getpParserCounter() {
        return pParserCounter;
    }

    @Override
    public ArrayList<Text> parse(String text, TextType type) {
        log.info("parsing to paragraphs");
        String[] str = text.trim().split(REGEX);
        ArrayList<Text> paragraphList = new ArrayList<>();
        if (type == TextType.PARAGRAPH){
            for (String s : str) {
                paragraphList.add(new Paragraph(s));
            }
        } else {
            Parser sParser = new SentenceParser();
            for (String s : str) {
                paragraphList.add(new Paragraph(sParser.parse(s, type)));
            }
        }
        log.info("parsing to paragraphs complete");
        return paragraphList;
    }
}
