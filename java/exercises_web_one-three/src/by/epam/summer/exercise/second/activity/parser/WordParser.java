package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Lexeme;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import by.epam.summer.exercise.second.entity.Word;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser implements Parser {

    private static int wParserCounter = 0;
    private final Pattern POSTREGEX = Pattern.compile("\\W+$");
    private final Pattern PREREGEX = Pattern.compile("^\\W+");
    private static final Logger log = Logger.getLogger(WordParser.class);

    public WordParser() {
    wParserCounter++;
    }

    public static int getwParserCounter() {
        return wParserCounter;
    }

    @Override
    public ArrayList<Text> parse(String text, TextType type) {
        log.info("parsing to words");
        ArrayList<Text> wordList = new ArrayList<>();
        Matcher postmatcher = POSTREGEX.matcher(text);
        Matcher prematcher = PREREGEX.matcher(text);
        Parser symParser = new SymbolParser();
        if (prematcher.find()) {
            String prePunctuation = prematcher.group();
            text = prematcher.replaceAll("");
            wordList.add(new Lexeme(symParser.parse(prePunctuation, TextType.SYMBOL)));
        }
        if (postmatcher.find()){
            String postPunctuation = postmatcher.group();
            text = postmatcher.replaceAll("");
            wordList.add(new Word(text));
            wordList.add(new Lexeme(symParser.parse(postPunctuation, TextType.SYMBOL)));
        } else{
            wordList.add(new Word(text));
        }
        log.info("parsing to words complete");
        return wordList;
    }

}
