package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Sentence;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser implements Parser {

    private static int sParserCounter = 0;
    private final String REGEX = "[.!?]+(\\u2028||\\t||\\s)+[^(A-Z)]*";
    private final Pattern pattern = Pattern.compile(REGEX);
    private static final Logger log = Logger.getLogger(SentenceParser.class);

    public SentenceParser() {
        sParserCounter++;
    }

    public static int getsParserCounter() {
        return sParserCounter;
    }

    @Override
    public ArrayList<Text> parse(String text, TextType type) {
        log.info("parsing to sentences");
        ArrayList<Text> sentenceList = new ArrayList<>();
        Matcher matcher = pattern.matcher(text);
        ArrayList<String> separatorList = new ArrayList<>();
        while (matcher.find()){
            separatorList.add(matcher.group());
        }
        String[] str = text.trim().split(REGEX);
        for (int i = 0; i < str.length; i++){
            str[i] = str[i].concat(separatorList.get(i)).trim();
        }

        if (type == TextType.SENTENCE){
            for (String s : str) {
                sentenceList.add(new Sentence(s));
            }
        } else {
            Parser lParser = new LexemeParser();
            for (String s : str) {
                sentenceList.add(new Sentence(lParser.parse(s, type)));
            }
        }
        log.info("parsing to sentences complete");
        return sentenceList;
    }
}
