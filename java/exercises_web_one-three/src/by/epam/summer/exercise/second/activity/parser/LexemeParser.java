package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Lexeme;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class LexemeParser implements Parser {

    private static int lParserCounter = 0;
    private final String REGEX = "\\s+";
    private static final Logger log = Logger.getLogger(LexemeParser.class);

    public LexemeParser() {
        lParserCounter++;
    }

    public static int getlParserCounter() {
        return lParserCounter;
    }

    @Override
    public ArrayList<Text> parse(String text, TextType type) {
        log.info("parsing to lexemes");
        String[] str = text.trim().split(REGEX);
        ArrayList<Text> lexemeList = new ArrayList<>();
        if (type == TextType.LEXEME){
            for (String s : str) {
                lexemeList.add(new Lexeme(s));
            }
        }else if (type == TextType.SYMBOL){
            Parser symParser = new SymbolParser();
            for (String s : str) {
                lexemeList.add(new Lexeme(symParser.parse(s, type)));
            }
        } else if (type == TextType.WORD){
            Parser wParser = new WordParser();
            for (String s : str) {
                lexemeList.add(new Lexeme(wParser.parse(s, type)));
            }
        }
        log.info("parsing to lexemes complete");
        return lexemeList;
    }
}
