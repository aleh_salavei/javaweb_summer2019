package by.epam.summer.exercise.second;

import by.epam.summer.exercise.second.activity.*;
import by.epam.summer.exercise.second.entity.GlobalText;
import by.epam.summer.exercise.second.entity.TextType;
import by.epam.summer.exercise.second.entity.UnsortableException;
import org.apache.log4j.Logger;

public class Runner {

    private static final Logger log = Logger.getLogger(Runner.class);

    public static void main(String[] args) throws CloneNotSupportedException {
        log.info("program starts");

        GlobalText gt = TextHandler.readText("second_exercise_content.txt");
        log.debug(gt.getOriginalContent());
        TextHandler.parseText(gt, TextType.WORD);
        try {
            log.debug(TextHandler.sortParagraphs(gt));
        } catch (UnsortableException e){
            log.error(e);
        }
        TextWriter.contentWriter("result_content.txt", TextHandler.generateText(gt.toString()));

        log.info("program ends");
    }
}
