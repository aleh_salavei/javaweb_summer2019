package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Objects;

public class Symbol implements Text {
    private Character content;

    public Symbol(char text) {
        this.content = text;
    }

    @Override
    public ArrayList<Text> getSeparatedText() {
        return new ArrayList<>();
    }

    @Override
    public String getContent() {
        return content.toString();
    }

    @Override
    public String toString() {
        return content.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Symbol)) return false;
        Symbol symbol = (Symbol) o;
        return Objects.equals(getContent(), symbol.getContent());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getContent());
    }
}
