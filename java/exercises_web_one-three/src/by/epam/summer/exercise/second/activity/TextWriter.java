package by.epam.summer.exercise.second.activity;

import org.apache.log4j.Logger;

import java.io.*;

public class TextWriter {

    private static final Logger log = Logger.getLogger(TextWriter.class);

    public static void contentWriter(String path, String[] text) {
        log.info("writing starts");
        String resourceFolder = System.getProperty("user.dir") + "\\src\\by\\epam\\summer\\exercise\\second\\resource\\";
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
                (new FileOutputStream(resourceFolder + path)))) {
            File newFile = new File(resourceFolder + path);
            if (!newFile.exists()){
                newFile.createNewFile();
            }
            for (int i = 0; i < text.length; i++) {
                bw.write("\t" + text[i]);
                if (i < text.length-1)
                    bw.newLine();
            }
        } catch (FileNotFoundException e) {
            log.error("there's no such file");
        } catch (IOException e) {
            log.error("the file is damaged");
        } finally {
            log.info("writing ends");
        }
    }
}
