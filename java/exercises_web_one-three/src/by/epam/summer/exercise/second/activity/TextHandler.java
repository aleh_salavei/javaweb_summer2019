package by.epam.summer.exercise.second.activity;

import by.epam.summer.exercise.second.activity.parser.ParagraphParser;
import by.epam.summer.exercise.second.activity.parser.Parser;
import by.epam.summer.exercise.second.entity.GlobalText;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import by.epam.summer.exercise.second.entity.UnsortableException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class TextHandler {

    private static final Logger log = Logger.getLogger(TextHandler.class);

    public static GlobalText readText(String path) {
        log.info("getting new text from file");
        return new GlobalText(TextReader.contentReader(path));
    }

    public static ArrayList<Text> parseText(GlobalText gt, TextType type){
        log.info("parsing whole text");
        Parser pParser = new ParagraphParser();
        gt.getSeparatedText().clear();
        gt.getSeparatedText().addAll(pParser.parse(gt.getOriginalContent(), type));
        gt.setHandledContent(gt.getSeparatedText().toString());
        return gt.getSeparatedText();
    }

    public static GlobalText sortParagraphs(GlobalText gt)throws CloneNotSupportedException, UnsortableException{
        log.info("sorting text by paragraph's length");
        GlobalText newGT = (GlobalText) gt.clone();
        if (!gt.getSeparatedText().get(0).getSeparatedText().isEmpty()) {
            ParagraphComparator comparator = new ParagraphComparator();
            newGT.getSeparatedText().sort(comparator);
        } else {
            throw new UnsortableException("trying to sort paragraph by sentence counts without parsing to sentences");
        }
        return newGT;
    }

    public static String[] generateText(String text){
        log.info("creating new text from parsed text");
        return text.trim().split("\\t");
    }
}
