package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Symbol;
import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class SymbolParser implements Parser {

    private static int symParserCounter = 0;
    private static final Logger log = Logger.getLogger(SymbolParser.class);

    public SymbolParser() {
        symParserCounter++;
    }

    public static int getSymParserCounter() {
        return symParserCounter;
    }

    @Override
    public ArrayList<Text> parse(String text, TextType type) {
        log.info("parsing to symbols");
        char[] ch = text.toCharArray();
        ArrayList<Text> symbolList = new ArrayList<>();
        for (char s : ch) {
            symbolList.add(new Symbol(s));
        }
        log.info("parsing to symbols complete");
        return symbolList;
    }
}
