package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Sentence implements Text {
    private ArrayList<Text> separatedText;
    private String content;

    public Sentence(String text){
        this.content = text;
        this.separatedText = new ArrayList<>();
    }

    public Sentence(ArrayList<Text> text) {
        separatedText = new ArrayList<>();
        separatedText.addAll(text);
        this.content = this.toString();
    }

    public ArrayList<Text> getSeparatedText() {
        return separatedText;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String toString(){
        Iterator<Text> iterator = separatedText.iterator();
        String result = "";
        while (iterator.hasNext())
            result = result.concat(" ").concat(iterator.next().getContent());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;
        Sentence sentence = (Sentence) o;
        return Objects.equals(getSeparatedText(), sentence.getSeparatedText()) &&
                Objects.equals(getContent(), sentence.getContent());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getSeparatedText(), getContent());
    }
}
