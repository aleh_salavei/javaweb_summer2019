package by.epam.summer.exercise.second.entity;

public enum TextType {
    PARAGRAPH, SENTENCE, LEXEME, WORD, SYMBOL
}
