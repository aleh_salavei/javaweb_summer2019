package by.epam.summer.exercise.second.activity;

import org.apache.log4j.Logger;

import java.io.*;

public class TextReader {

    private static final Logger log = Logger.getLogger(TextReader.class);

    public static String contentReader(String path) {
        log.info("reading starts");
        String paragraph = "";
        String resourceFolder = System.getProperty("user.dir") + "\\src\\by\\epam\\summer\\exercise\\second\\resource\\";
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(resourceFolder + path)))) {
            String str;
            while ((str = bf.readLine()) != null) {
                paragraph = paragraph.concat("\t").concat(str);
            }
        } catch (FileNotFoundException e) {
            log.error("there's no such file");
        } catch (IOException e) {
            log.error("the file is damaged");
        } finally {
            log.info("reading ends");
        }
        return paragraph;
    }
}
