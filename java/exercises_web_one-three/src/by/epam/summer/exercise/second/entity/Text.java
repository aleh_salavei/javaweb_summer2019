package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;

public interface Text {

    ArrayList<Text> getSeparatedText();
    String getContent();
    String toString();

}
