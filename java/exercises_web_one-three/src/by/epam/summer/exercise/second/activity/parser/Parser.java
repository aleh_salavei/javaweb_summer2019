package by.epam.summer.exercise.second.activity.parser;

import by.epam.summer.exercise.second.entity.Text;
import by.epam.summer.exercise.second.entity.TextType;

import java.util.ArrayList;

public interface Parser {

    ArrayList<Text> parse(String text, TextType type);

}
