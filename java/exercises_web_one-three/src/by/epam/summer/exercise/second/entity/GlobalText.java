package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class GlobalText implements Text, Cloneable {
    private ArrayList<Text> separatedText;
    private String originalContent;
    private String handledContent;

    public GlobalText(String text){
        this.originalContent = text;
        this.handledContent = "";
        this.separatedText = new ArrayList<>();
    }

    public String getOriginalContent() {
        return originalContent;
    }

    @Override
    public String getContent() {
        return handledContent;
    }

    public void setHandledContent(String handledContent) {
        this.handledContent = handledContent;
    }

    @Override
    public ArrayList<Text> getSeparatedText() {
        return separatedText;
    }

    @Override
   public String toString(){
       Iterator<Text> iterator = separatedText.iterator();
       String result = "";
       while (iterator.hasNext())
           result = result.concat("\t").concat(iterator.next().getContent().trim());
       return result;
   }

    @Override
    public Object clone() throws CloneNotSupportedException {
        GlobalText cloned = (GlobalText)super.clone();
        cloned.separatedText = (ArrayList<Text>)cloned.getSeparatedText().clone();
        return cloned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GlobalText)) return false;
        GlobalText that = (GlobalText) o;
        return Objects.equals(getSeparatedText(), that.getSeparatedText()) &&
                Objects.equals(getOriginalContent(), that.getOriginalContent()) &&
                Objects.equals(handledContent, that.handledContent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(getSeparatedText(), getOriginalContent(), handledContent);
    }
}
