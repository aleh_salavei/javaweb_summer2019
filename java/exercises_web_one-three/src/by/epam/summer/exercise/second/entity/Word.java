package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Objects;

public class Word implements Text {
    private String content;

    public Word(String text) {
        this.content = text;
    }

    @Override
    public ArrayList<Text> getSeparatedText() {
        return new ArrayList<>();
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word = (Word) o;
        return Objects.equals(getContent(), word.getContent());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getContent());
    }
}
