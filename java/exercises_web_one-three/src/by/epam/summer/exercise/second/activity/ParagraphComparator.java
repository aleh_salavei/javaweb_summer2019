package by.epam.summer.exercise.second.activity;

import by.epam.summer.exercise.second.entity.Text;

import java.util.Comparator;

public class ParagraphComparator implements Comparator<Text> {

    @Override
    public int compare(Text o1, Text o2) {
        return Integer.compare(o1.getSeparatedText().size(), o2.getSeparatedText().size());
    }
}
