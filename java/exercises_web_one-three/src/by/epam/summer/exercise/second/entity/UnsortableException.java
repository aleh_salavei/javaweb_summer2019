package by.epam.summer.exercise.second.entity;

public class UnsortableException extends Exception {

    public UnsortableException(String message){
        super(message);
    }
}
