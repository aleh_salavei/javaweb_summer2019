package by.epam.summer.exercise.second.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Lexeme implements Text {
    private ArrayList<Text> separatedText;
    private String content;

    public Lexeme(String text){
        this.content = text;
        this.separatedText = new ArrayList<>();
    }

    public Lexeme(ArrayList<Text> text) {
        separatedText = new ArrayList<>();
        separatedText.addAll(text);
        this.content = this.toString();
    }

    public ArrayList<Text> getSeparatedText() {
        return separatedText;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String toString(){
        Iterator<Text> iterator = separatedText.iterator();
        String result = "";
        while (iterator.hasNext())
        result = result.concat(iterator.next().getContent());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lexeme)) return false;
        Lexeme lexeme = (Lexeme) o;
        return Objects.equals(getSeparatedText(), lexeme.getSeparatedText()) &&
                Objects.equals(getContent(), lexeme.getContent());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getSeparatedText(), getContent());
    }
}
