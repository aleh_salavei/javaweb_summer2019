package by.epam.summer.exercise.first.entity.toy;

public enum ToyType {
    BALL, CAR, DOLL, BRICKS
}
