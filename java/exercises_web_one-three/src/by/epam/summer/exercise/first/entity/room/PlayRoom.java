package by.epam.summer.exercise.first.entity.room;

import by.epam.summer.exercise.first.entity.toy.*;

import java.util.ArrayList;
import java.util.Objects;

public class PlayRoom {
    private double availableAmount;
    private ArrayList<AbstractToy> toyList;
    private String name;

    public PlayRoom(double availableAmount, String name){
        this.availableAmount = availableAmount;
        this.name = name;
        toyList = new ArrayList<>();
    }

    public double getAvailableAmount() {
        return availableAmount;
    }

    public ArrayList<AbstractToy> getToyList() {
        return toyList;
    }

    public String getName() {
        return name;
    }

    public void setAvailableAmount(double availableAmount) {
        this.availableAmount = availableAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayRoom room = (PlayRoom) o;
        return availableAmount == room.availableAmount &&
                Objects.equals(name, room.name) &&
                toyList.equals(room.toyList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, availableAmount, toyList);
    }
}
