package by.epam.summer.exercise.first.entity.toy;

public class CarToy extends AbstractToy {
    public enum CarType{SPORTCAR, TRUCK, BUILDINGCAR, SIMPLECAR}
    CarType carType;
    boolean hasMotor;

    public CarToy(ToySize size, String name, double price, boolean hasMotor) {
        super(ToyType.CAR, size, name, price);
        carType = CarType.SIMPLECAR;
        this.hasMotor = hasMotor;
    }

    public boolean isHasMotor() {
        return hasMotor;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @Override
    public void action(){
        System.out.println(name + " is riding and drifting");
    }
}
