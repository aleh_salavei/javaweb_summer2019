package by.epam.summer.exercise.first.entity.toy;

public class DollToy extends AbstractToy {
    public enum DollType {SOFTDOLL, PLASTICDOLL, ROBOT}
    DollType dollType;
    boolean hasAI;
    String brandName;

     public DollToy(ToySize size, String name, double price, String brandName, boolean hasAI) {
        super(ToyType.DOLL, size, name, price);
         dollType = DollType.PLASTICDOLL;
         this.brandName = brandName;
         this.hasAI = hasAI;
    }

    public boolean isHasAI() {
        return hasAI;
    }

    public String getBrandName() {
        return brandName;
    }

    public DollType getDollType() {
        return dollType;
    }

    public void setDollType(DollType dollType) {
        this.dollType = dollType;
    }

    @Override
    public void action() {
        System.out.println(name + " is sitting in the toy house");
    }
}
