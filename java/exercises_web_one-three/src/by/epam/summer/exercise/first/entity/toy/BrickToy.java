package by.epam.summer.exercise.first.entity.toy;

public class BrickToy extends AbstractToy {
    String brandName;
    String productLineName;
    boolean isConstructor;

    public BrickToy(ToySize size, String name, double price, String brandName, String productLineName,
                    boolean isConstructor) {
        super(ToyType.BRICKS, size, name, price);
        this.brandName = brandName;
        this.productLineName = productLineName;
        this.isConstructor = isConstructor;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getProductLineName() {
        return productLineName;
    }

    public boolean isConstructor() {
        return isConstructor;
    }

    @Override
    public void action() {
        System.out.println(name + " is making toy house");
    }
}
