package by.epam.summer.exercise.first.entity.toy;

import java.util.Objects;

public abstract class AbstractToy {
    String name;
    ToyType type;
    double price;
    ToySize size;

    AbstractToy(ToyType type, ToySize size, String name, double price){
        this.type = type;
        this.size = size;
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public ToyType getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public ToySize getSize() {
        return size;
    }

    abstract void action();

    @Override
    public String toString() {
        return size + " " + type + " " + name + " " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractToy toy = (AbstractToy) o;
        return price == toy.price &&
                Objects.equals(name, toy.name) &&
                size == toy.size && type == toy.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size, type, price);
    }
}
