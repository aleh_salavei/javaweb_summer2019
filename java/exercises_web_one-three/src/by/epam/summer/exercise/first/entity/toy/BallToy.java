package by.epam.summer.exercise.first.entity.toy;

public class BallToy extends AbstractToy {
    public enum BallType{BASKETBALL, FOOTBALL, VOLLEYBALL, BEACHBALL, SIMPLENBALL}
    BallType ballType;
    String material;

    public BallToy(ToySize size, String name, double price, String material) {
        super(ToyType.BALL, size, name, price);
        ballType = BallType.SIMPLENBALL;
        this.material = material;
    }

    public String getMaterial() {
        return material;
    }

    public BallType getBallType() {
        return ballType;
    }

    public void setBallType(BallType ballType) {
        this.ballType = ballType;
    }

    @Override
    public void action(){
        System.out.println(name + " is rolling and jumping");
    }

}
