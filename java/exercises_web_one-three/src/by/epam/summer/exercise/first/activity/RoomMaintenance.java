package by.epam.summer.exercise.first.activity;

import by.epam.summer.exercise.first.entity.exception.NotEnoughMoneyException;
import by.epam.summer.exercise.first.entity.room.PlayRoom;
import by.epam.summer.exercise.first.entity.toy.*;
import org.apache.log4j.Logger;

import java.util.*;

public class RoomMaintenance {
    private ArrayList<AbstractToy> toyCatalog = new ArrayList<>();
    private static final Logger logger = Logger.getLogger(RoomMaintenance.class);

    public void fillCatalog(String path) {
        logger.info("creating the catalog");
        String[][] toysFromFile = TxtFileReader.getContent(path);
        for (String[] aToysFromFile : toysFromFile) {
            try {
                switch (ToyType.valueOf(aToysFromFile[0].toUpperCase())) {
                    case BALL:
                        toyCatalog.add(new BallToy(ToySize.valueOf(aToysFromFile[1].toUpperCase()),
                                aToysFromFile[2], Double.parseDouble(aToysFromFile[3]), aToysFromFile[4]));
                        break;
                    case BRICKS:
                        toyCatalog.add(new BrickToy(ToySize.valueOf(aToysFromFile[1].toUpperCase()),
                                aToysFromFile[2], Double.parseDouble(aToysFromFile[3]), aToysFromFile[4],
                                aToysFromFile[5], Boolean.parseBoolean(aToysFromFile[6])));
                        break;
                    case CAR:
                        toyCatalog.add(new CarToy(ToySize.valueOf(aToysFromFile[1].toUpperCase()),
                                aToysFromFile[2], Double.parseDouble(aToysFromFile[3]),
                                Boolean.parseBoolean(aToysFromFile[4])));
                        break;
                    case DOLL:
                        toyCatalog.add(new DollToy(ToySize.valueOf(aToysFromFile[1].toUpperCase()),
                                aToysFromFile[2], Double.parseDouble(aToysFromFile[3]), aToysFromFile[4],
                                Boolean.parseBoolean(aToysFromFile[5])));
                        break;
                }
                logger.info("toy added to the catalog");
            }
            catch(IllegalArgumentException e){
                logger.error("data error in .txt file");
            }
            catch(ArrayIndexOutOfBoundsException e){
                logger.error("not enough information about a toy");
            }
        }
    }

    public ArrayList<AbstractToy> getToyCatalog() {
        return toyCatalog;
    }

    public void showCatalog(){
        logger.info("printing the catalog");
        for (AbstractToy toy: toyCatalog) {
            logger.debug(toy);
        }
    }

    private Optional<AbstractToy> getToyFromCatalog(String toyName) {
        logger.info("searching for toy in the catalog");
        AbstractToy toy;
        for (AbstractToy aToyCatalog : toyCatalog) {
            toy = aToyCatalog;
            if (toy.getName().equals(toyName)) {
                logger.info("searching successfully complete");
                return Optional.of(toy);
            }
        }
        logger.error("searching failed");
       return Optional.empty();
    }

    public PlayRoom createRoom(double availableAmount, String name){
        logger.info("creating new playing room");
        return new PlayRoom(availableAmount, name);
    }

    public void addToy(PlayRoom room, String toyName) throws NotEnoughMoneyException {
        logger.info("adding toy to the room");
        if (getToyFromCatalog(toyName).isPresent()) {
            AbstractToy toy = getToyFromCatalog(toyName).get();
            if (room.getAvailableAmount() >= toy.getPrice()) {
                room.getToyList().add(toy);
                room.setAvailableAmount(room.getAvailableAmount() - toy.getPrice());
                logger.info("toy successfully added");
            } else {
                logger.info("adding failed");
                throw new NotEnoughMoneyException("There are only " + room.getAvailableAmount() + "$ available. Please choose cheaper toy.");
            }
        } else {
            logger.error("adding failed");
        }
    }

    public void deleteToy(PlayRoom room, String toyName){
        logger.info("deleting toy from the room");
        if (getToyFromCatalog(toyName).isPresent()) {
            AbstractToy toy = getToyFromCatalog(toyName).get();
            for (AbstractToy abstractToy : room.getToyList()) {
                if (toy.equals(abstractToy)) {
                    room.getToyList().remove(abstractToy);
                    logger.info("toy successfully deleted");
                    break;
                }
            }
        }
        else {
            logger.error("deleting failed");
        }
    }

    public void showRoom(PlayRoom room){
        logger.info("printing list of toys in the room");
        for (AbstractToy toy: room.getToyList()) {
            logger.debug(toy);
        }
    }

    public Optional<AbstractToy> getToyFromRoom(PlayRoom room, String name){
        logger.info("searching for the toy in the room by name");
        AbstractToy toy;
        for (AbstractToy abstractToy : room.getToyList()) {
            toy = abstractToy;
            if (toy.getName().equals(name)) {
                logger.info("searching successfully completed");
                return Optional.of(toy);
            }
        }
        logger.error("searching failed");
        return Optional.empty();
    }
    public ArrayList<AbstractToy> getToyFromRoom(PlayRoom room, ToyType type, ToySize size){
        logger.info("searching for the toys in the room by type and size");
        AbstractToy toy;
        ArrayList<AbstractToy> toys = new ArrayList<>();
        for (AbstractToy abstractToy : room.getToyList()) {
            toy = abstractToy;
            if (toy.getType().equals(type) && toy.getSize().equals(size)) {
                toys.add(toy);
            }
        }
        logger.info("searching complete");
        return toys;
    }

    public ArrayList<AbstractToy> getToyFromRoom(PlayRoom room, ToyType type){
        logger.info("searching for the toys in the room by type");
        AbstractToy toy;
        ArrayList<AbstractToy> toys = new ArrayList<>();
        for (AbstractToy abstractToy : room.getToyList()) {
            toy = abstractToy;
            if (toy.getType().equals(type)) {
                toys.add(toy);
            }
        }
        logger.info("searching complete");
        return toys;
    }

    public ArrayList<AbstractToy> getToyFromRoom(PlayRoom room, double maxPrice){
        logger.info("searching for the toys in the room by price");
        AbstractToy toy;
        ArrayList<AbstractToy> toys = new ArrayList<>();
        for (AbstractToy abstractToy : room.getToyList()) {
            toy = abstractToy;
            if (toy.getPrice() <= maxPrice) {
                toys.add(toy);
            }
        }
        logger.info("searching complete");
        return toys;
    }

    public void sortToysByPrice(PlayRoom room){
        logger.info("sorting toys by price");
        Comparator<AbstractToy> comparator = Comparator.comparing(AbstractToy::getPrice);
        room.getToyList().sort(comparator);
        logger.info("sorting complete");
    }

    public void sortToysByTypeAndSize(PlayRoom room){
        logger.info("sorting toys by type and size");
        Comparator<AbstractToy> comparator = Comparator.comparing(AbstractToy::getType);
        comparator = comparator.thenComparing(AbstractToy::getSize);
        room.getToyList().sort(comparator);
        logger.info("sorting complete");
    }
}
