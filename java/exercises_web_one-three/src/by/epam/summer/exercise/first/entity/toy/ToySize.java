package by.epam.summer.exercise.first.entity.toy;

public enum ToySize {
    LITTLE, MIDDLE, BIG
}
