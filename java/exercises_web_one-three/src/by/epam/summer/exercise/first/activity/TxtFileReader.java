package by.epam.summer.exercise.first.activity;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TxtFileReader {

    private static final Logger log = Logger.getLogger(TxtFileReader.class);

    private static ArrayList<String> readFile(String path){
        log.info("reading starts.");
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader br;
        String resourceFolder = System.getProperty("user.dir") + "\\src\\by\\epam\\summer\\exercise\\first\\resource\\";
        try {
            br = new BufferedReader(new FileReader(resourceFolder + path));
            String content = br.readLine();
            while (content != null) {
                lines.add(content);
                content = br.readLine();
            }
            br.close();
        }
        catch (FileNotFoundException e){
            log.info("there is no such file");
        } catch (IOException e) {
            log.info("wrong information in file");
        } finally {
            log.info("reading ends.");
        }
        return lines;
    }

    public static String[][] getContent(String path){
        ArrayList<String> lines = readFile(path);
        String[][] content = new String[lines.size()][];

        for (int i = 0; i < content.length; i++) {
            content[i] = lines.get(i).split(", ");
        }
        return content;
    }
}
