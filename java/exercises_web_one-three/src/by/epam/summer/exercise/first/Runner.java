package by.epam.summer.exercise.first;

import org.apache.log4j.Logger;
import by.epam.summer.exercise.first.activity.RoomMaintenance;
import by.epam.summer.exercise.first.entity.exception.NotEnoughMoneyException;
import by.epam.summer.exercise.first.entity.room.PlayRoom;
import by.epam.summer.exercise.first.entity.toy.ToySize;
import by.epam.summer.exercise.first.entity.toy.ToyType;

public class Runner {

    private static final Logger log = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        log.info("program starts");
        RoomMaintenance rm = new RoomMaintenance();

        rm.fillCatalog("first_exercise_content.txt");
        rm.showCatalog();

        PlayRoom room = rm.createRoom(50, "UniAge #101");
        try {
            rm.addToy(room, "red ball");
            rm.addToy(room, "ferrari");
            rm.addToy(room, "beach ball");
            rm.addToy(room, "lego");
            rm.addToy(room, "green ball");
            rm.addToy(room, "china's toys");
        }
        catch (NotEnoughMoneyException e){
            log.info(e);
        }
        rm.showRoom(room);

        rm.sortToysByPrice(room);
        rm.showRoom(room);
        rm.sortToysByTypeAndSize(room);
        rm.showRoom(room);


        log.debug(rm.getToyFromRoom(room, 6));
        log.debug(rm.getToyFromRoom(room, ToyType.BALL));
        log.debug(rm.getToyFromRoom(room, ToyType.BALL, ToySize.BIG));
        log.debug(rm.getToyFromRoom(room, "ferrari"));

        rm.deleteToy(room, "lego");
        rm.showRoom(room);

        log.info("program ends");
    }
}
