var n = 0;
var x = 0;
var emailValid = false;
var ageValid = false;
var phoneValid = false;
var loginValid = false;
var passValid = false;
var repassValid = false;

function addEmail() {
    if (n < 3 && emailValid) {
        var root = document.getElementById('email_item');
        var table = document.createElement('table');
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        var td2 = document.createElement('td');
        var input = document.createElement('input');
        var bt = document.createElement('input');
        var p = document.createElement('p');
        var localEmailValid = emailValid;

        p.innerHTML = 'Enter alternative e-mail';
        p.id = 'extra_p' + x;
        table.id = 'extra_table' + x;
        input.class = 'extra email' + x;
        input.id = 'extra_email' + x;
        input.name = 'extra email' + x;
        input.placeholder = 'Extra e-mail';
        input.required = 'required';
        input.type = 'email';
        input.onkeyup = function () {
            validateEmail(input.id, 'keyup');
        };
        input.onblur = function () {
            validateEmail(this.id, 'blur');
        };

        bt.class = 'button-delete';
        bt.id = 'delete';
        bt.type = 'button';
        bt.onclick = function () {
            minus(table.id, p.id);
            emailValid = localEmailValid;
            validateSummary();
        };

        root.appendChild(p);
        var activeRoot = root.appendChild(table);
        activeRoot = activeRoot.appendChild(tr);
        var activeTD = activeRoot.appendChild(td);
        activeTD.appendChild(input);
        activeTD = activeRoot.appendChild(td2);
        activeTD.appendChild(bt);
        emailValid = false;

        x++;
        n++;
    }
}

function minus(table_id, p_id) {
    var del_root = document.getElementById(table_id);
    del_root.remove();
    del_root = document.getElementById(p_id);
    del_root.remove();
    n--;
}

function validateEmail(id, mode) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var val = document.getElementById(id).value;
    if (reg.test(val) === false) {
        if (mode === "blur") {
            document.getElementById(id).style.color = 'red';
            emailValid = false;
            document.getElementById('send').disabled = 1;
            return false;
        }
    } else {
        document.getElementById(id).style.color = '';
        emailValid = true;
        validateSummary();
        return true;
    }
}

function validateAge(id, mode) {
    var val = document.getElementById(id).value;
    if (val < 7 || val > 120) {
        if (mode === "blur") {
            document.getElementById(id).style.color = 'red';
            ageValid = false;
            document.getElementById('send').disabled = 1;
            return false;
        }
    } else {
        document.getElementById(id).style.color = '';
        ageValid = true;
        validateSummary();
        return true;
    }
}

function validatePass(id, mode) {
    var reg = /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,}/;
    var val = document.getElementById(id).value;
    if (reg.test(val) === false) {
        if (mode === "blur") {
            document.getElementById(id).style.color = 'red';
            passValid = false;
            document.getElementById('send').disabled = 1;
            return false;
        }
    } else {
        document.getElementById(id).style.color = '';
        passValid = true;
        validateSummary();
        return true;
    }
}

function validateRePass(id) {
    var original = document.getElementById('pass').value;
    var val = document.getElementById(id).value;
    if (original !== val) {
        document.getElementById(id).style.color = 'red';
        repassValid = false;
        document.getElementById('send').disabled = 1;
        return false;
    } else {
        document.getElementById(id).style.color = '';
        repassValid = true;
        validateSummary();
        return true;
    }
}

function validateLogin(id, mode) {
    var reg = /^([A-Za-z])[\w+]{4,}/;
    var val = document.getElementById(id).value;
    if (reg.test(val) === false) {
        if (mode === "blur") {
            document.getElementById(id).style.color = 'red';
            loginValid = false;
            document.getElementById('send').disabled = 1;
            return false;
        }
    } else {
        document.getElementById(id).style.color = '';
        loginValid = true;
        validateSummary();
        return true;
    }
}

function validatePhone(id, mode) {
    var reg = /^[\d+]{10,}$/;
    var val = document.getElementById(id).value;
    if (reg.test(val) === false) {
        if (mode === "blur") {
            document.getElementById(id).style.color = 'red';
            phoneValid = false;
            document.getElementById('send').disabled = 1;
            return false;
        }
    } else {
        document.getElementById(id).style.color = '';
        phoneValid = true;
        validateSummary();
        return true;
    }
}

function validateSummary() {
    if (emailValid && ageValid && phoneValid && loginValid && passValid && repassValid) {
        document.getElementById('send').disabled = 0;
    }

}